package com.katarzynaiwonakrzemien.togetherpay

import android.app.Application
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.togetherpay.di.AppComponent
import com.katarzynaiwonakrzemien.togetherpay.di.DaggerAppComponent
import com.katarzynaiwonakrzemien.togetherpay.log.AndroidLog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class App : Application(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Any>
    private val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder().appContext(this).build()
    }

    override fun onCreate() {
        Log.add(AndroidLog())
        appComponent.inject(this)
        super.onCreate()
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingActivityInjector

    companion object {
        var date: String = SimpleDateFormat("EEE  |  dd MMM yyyy", Locale.getDefault())
            .format(Calendar.getInstance().time)
    }

}