package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.details

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsContract
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.togetherpay.BaseFragment
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.SelectedBudgetActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.user.UserBudgetActivity
import kotlinx.android.synthetic.main.fragment_budget_details.*

class BudgetDetailsFragment :
    BaseFragment<BudgetDetailsContract.Presenter, BudgetDetailsContract.Action, BudgetDetailsContract.Event>(),
    BudgetDetailsContract.View {

    override val layoutRes: Int = R.layout.fragment_budget_details
    private lateinit var idBudget: String
    private lateinit var budget: BudgetUi

    override fun consume(event: BudgetDetailsContract.Event) = with(event) {
        when (this) {
            is BudgetDetailsContract.Event.RenderBudget -> renderBudget(budget)
        }
    }

    private fun renderBudget(budget: BudgetUi) {
        otherUser.text = budget.otherUser.name
        nameInput.setText(budget.name)
        this.budget = budget
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        idBudget = arguments?.getString(SelectedBudgetActivity.KEY_ID_BUDGET)
            ?: throw(IllegalArgumentException(getString(R.string.errorNoBudgetId)))

        clear.setOnClickListener {
            AlertDialog.Builder(requireContext()).apply {
                setIcon(R.drawable.ic_clear_dark)
                setTitle(R.string.clearBudget)
                setMessage(getString(R.string.doYouWantClearBudget, nameInput.text.toString()))
                setPositiveButton(R.string.clear) { dialog, _ ->
                    send(BudgetDetailsContract.Action.ClearBudget(budget))
                    dialog.dismiss()
                }
                setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                create()
                show()
            }
        }

        send(BudgetDetailsContract.Action.FetchBudget(idBudget))

        delete.setOnClickListener {
            AlertDialog.Builder(requireContext()).apply {
                setIcon(R.drawable.ic_delete_dark)
                setTitle(R.string.deleteBudget)
                setMessage(getString(R.string.doYouWantDeleteBudget, nameInput.text.toString()))
                setPositiveButton(R.string.delete) { dialog, _ ->
                    send(BudgetDetailsContract.Action.DeleteBudget(budget))
                    dialog.dismiss()
                    startActivity(UserBudgetActivity.getIntent(requireContext()))
                }
                setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                create()
                show()
            }
        }

        revert.setOnClickListener { renderBudget(budget) }

        save.setOnClickListener {
            if (nameInput.text.isNullOrBlank()) {
                nameInputLayout.error = getString(R.string.errorNoBudgetName)
            } else {
                send(BudgetDetailsContract.Action.UpdateBudget(nameInput.text.toString(), budget))
                nameInputLayout.error = null
            }
        }
    }
}
