package com.katarzynaiwonakrzemien.togetherpay.presentation.user.budgets

import android.content.res.TypedArray
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericListAdapter
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericViewHolder

class BudgetSelectAdapter(
    itemCallback: DiffUtil.ItemCallback<BudgetUi>,
    private val budgetActionListener: BudgetActionListener
) : GenericListAdapter<BudgetUi>(itemCallback, R.layout.element_budget) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BudgetViewHolder =
        BudgetViewHolder(super.inflate(parent), budgetActionListener)

    class BudgetViewHolder(view: View, private val budgetActionListener: BudgetActionListener) :
        GenericViewHolder<BudgetUi>(view) {
        private val otherUser: TextView = view.findViewById(R.id.otherUser)
        private val name: TextView = view.findViewById(R.id.name)
        private val debt: TextView = view.findViewById(R.id.debt)
        private val typedArray: TypedArray = view.context.theme.obtainStyledAttributes(R.styleable.ViewStyle)

        override fun bindTo(item: BudgetUi) {
            name.text = item.name
            otherUser.text = item.otherUser.name
            if (item.debt.amount == 0.0) {
                debt.text = view.context.getString(R.string.noDebts)
                debt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null)
                view.setBackgroundColor(
                    typedArray.getColor(
                        R.styleable.ViewStyle_noDebtBudgetBackground,
                        Color.TRANSPARENT
                    )
                )
            } else {
                debt.text = item.debt.amount.toRoundedString()
                debt.setCompoundDrawablesWithIntrinsicBounds(
                    view.context.getDrawable(if (item.debt.isBelongToSignInUser) R.drawable.ic_debt_right else R.drawable.ic_debt_left),
                    null,
                    null,
                    null
                )
                view.setBackgroundColor(
                    if (item.debt.isBelongToSignInUser) typedArray.getColor(
                        R.styleable.ViewStyle_debtBudgetBackground,
                        Color.TRANSPARENT
                    ) else typedArray.getColor(
                        R.styleable.ViewStyle_owingBudgetBackground,
                        Color.TRANSPARENT
                    )
                )
            }
            view.setOnClickListener { budgetActionListener.onBudgetClicked(item) }
        }
    }

    interface BudgetActionListener {
        fun onBudgetClicked(budget: BudgetUi)
    }
}
