package com.katarzynaiwonakrzemien.togetherpay.presentation.start

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.katarzynaiwonakrzemien.domain.contract.start.StartContract
import com.katarzynaiwonakrzemien.togetherpay.BaseActivity
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.presentation.user.UserBudgetActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.acitivity_start.*
import javax.inject.Inject

class StartActivity :
    BaseActivity<StartContract.Presenter, StartContract.Action, StartContract.Event>(),
    StartContract.View, HasAndroidInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    private var signOut: Boolean = false
    private val googleClient: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(
            this,
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        )
    }

    override fun consume(event: StartContract.Event) = TODO()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setupSignOut(savedInstanceState)
        setContentView(R.layout.acitivity_start)
        googleSignIn.setOnClickListener { signIn() }
    }

    private fun signIn() {
        startActivityForResult(googleClient.signInIntent, RC_SIGN_IN)
    }

    private fun signOut() {
        googleClient.signOut()
        send(StartContract.Action.SignedOutUser)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RC_SIGN_IN -> handleSignInResult(
                GoogleSignIn.getSignedInAccountFromIntent(data)
            )
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun loggedInFlow(account: GoogleSignInAccount) {
        send(StartContract.Action.AddSignedInUser(account.id!!, account.displayName!!))
        startActivity(UserBudgetActivity.getIntent(this))
        finish()
    }

    private fun handleSignInResult(task: Task<GoogleSignInAccount>) {
        try {
            task.getResult(ApiException::class.java)?.let(::loggedInFlow)
        } catch (e: ApiException) {
            Snackbar.make(
                container,
                if (e.message == null) getString(R.string.errorLogin, e.message)
                else getString(R.string.errorSomething),
                Snackbar.LENGTH_LONG
            ).show()
        }
    }

    private fun setupSignOut(savedInstanceState: Bundle?) {
        signOut = if (savedInstanceState?.containsKey(KEY_SIGN_OUT) == true) {
            savedInstanceState.getBoolean(KEY_SIGN_OUT, false)
        } else {
            intent.getBooleanExtra(KEY_SIGN_OUT, false)
        }

        if (signOut) signOut()
    }

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    companion object {
        const val KEY_SIGN_OUT = "sign_out"
        const val RC_SIGN_IN = 1
        fun getIntent(context: Context, signOut: Boolean): Intent =
            Intent(context, StartActivity::class.java).apply {
                putExtra(KEY_SIGN_OUT, signOut)
            }
    }
}