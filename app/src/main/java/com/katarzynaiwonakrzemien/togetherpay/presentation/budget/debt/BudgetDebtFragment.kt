package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.debt

import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.appcompat.app.AlertDialog
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtContract
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.togetherpay.BaseFragment
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericDiffUtilCallback
import com.katarzynaiwonakrzemien.togetherpay.extension.clearFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.extension.requestFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.SelectedBudgetActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_budget_debt.*

class BudgetDebtFragment :
    BaseFragment<BudgetDebtContract.Presenter, BudgetDebtContract.Action, BudgetDebtContract.Event>(),
    BudgetDebtContract.View, BudgetDebtAdapter.BudgetDebtConfirmActionListener {

    private val adapter = BudgetDebtAdapter(GenericDiffUtilCallback(), this)
    override val layoutRes: Int = R.layout.fragment_budget_debt
    private lateinit var idBudget: String
    private var debtAmount: Double? = null

    override fun consume(event: BudgetDebtContract.Event) = with(event) {
        when (this) {
            is BudgetDebtContract.Event.RenderCurrentDebt -> renderCurrentDebt(debtInfo)
            is BudgetDebtContract.Event.RenderRepaidDebts -> renderRepaidDebts(debts)
            is BudgetDebtContract.Event.RefreshDone -> refreshDone()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        idBudget = arguments?.getString(SelectedBudgetActivity.KEY_ID_BUDGET)
            ?: throw(IllegalArgumentException(getString(R.string.errorNoBudgetId)))
        send(BudgetDebtContract.Action.FetchCurrentDebt(idBudget))
        send(BudgetDebtContract.Action.FetchRepaidDebts(idBudget))
        setupRepayButtons()
        swipeRefresh.setOnRefreshListener { send(BudgetDebtContract.Action.Refresh) }
        recycler.adapter = adapter
    }

    private fun setupRepayButtons() {
        repayClosed.setOnClickListener {
            repayGroup.visibility = VISIBLE
            repayClosed.visibility = GONE
        }
        repayOpened.setOnClickListener {
            repayGroup.visibility = GONE
            repayClosed.visibility = VISIBLE
        }
        repayAmountButton.setOnClickListener { showPayBackDialog() }
        repayAllButton.setOnClickListener {
            send(BudgetDebtContract.Action.AddDebt(idBudget, debtAmount!!))
        }
    }

    private fun showPayBackDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_pay_back, null)
        val amount = dialogView.findViewById<TextInputEditText>(R.id.amountInput)
            .apply { requestFocusAndShowKeyboard() }
        val amountLayout: TextInputLayout = dialogView.findViewById(R.id.amountInputLayout)

        AlertDialog.Builder(requireContext()).apply {
            setView(dialogView)
            setIcon(R.drawable.ic_repay_debt)
            setTitle(R.string.payBackSomeMoney)
            setPositiveButton(R.string.repay) { _, _ -> }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                amount.clearFocusAndShowKeyboard()
            }
            create()
        }.show().apply {
            getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                when {
                    amount.text.isNullOrBlank() -> {
                        amountLayout.error = getString(R.string.errorNoAmount)
                    }
                    amount.text.toString().toDoubleOrNull()!! > debtAmount!! -> {
                        amountLayout.error = getString(R.string.yourDebt, debtAmount!!)
                    }
                    else -> {
                        send(
                            BudgetDebtContract.Action.AddDebt(
                                amount = amount.text.toString().toDoubleOrNull()!!,
                                idBudget = idBudget
                            )
                        )
                        amount.clearFocusAndShowKeyboard()
                        dismiss()
                    }
                }
            }
        }
    }

    private fun renderCurrentDebt(debt: AmountInfoUi?) {
        if (debt == null) {
            repayGroup.visibility = GONE
            repayClosed.visibility = GONE
        } else {
            if (debt.isBelongToSignInUser) {
                repayClosed.visibility = VISIBLE
                info.text =
                    if (debt.amount != 0.0) getString(R.string.yourDebt, debt.amount)
                    else getString(R.string.noDebts)

            } else {
                repayClosed.visibility = GONE
                repayGroup.visibility = GONE
                info.text =
                    if (debt.amount != 0.0) getString(R.string.someoneDebtIs, debt.amount)
                    else getString(R.string.noDebts)
            }
            debtAmount = debt.amount
        }
    }

    private fun renderRepaidDebts(debts: List<DebtUi>) {

        adapter.submitList(debts)
    }

    private fun refreshDone() {
        swipeRefresh.isRefreshing = false
    }

    override fun onDebtConfirmClicked(debt: DebtUi) {
        send(BudgetDebtContract.Action.ConfirmDebt(debt))
    }

    override fun onDebtRejectClicked(debt: DebtUi) {
        send(BudgetDebtContract.Action.RejectDebt(debt))
    }

    override fun onDebtDeleteClicked(idDebt: String) {
        send(BudgetDebtContract.Action.DeleteDebt(idDebt))
    }

    override fun onDebtRepayAgainClicked(debt: DebtUi) {
        send(BudgetDebtContract.Action.RepayAgainDebt(debt))
    }
}