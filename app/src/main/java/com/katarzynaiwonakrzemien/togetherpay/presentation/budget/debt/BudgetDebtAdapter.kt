package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.debt

import android.content.res.TypedArray
import android.graphics.Color
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageButton
import androidx.constraintlayout.widget.Group
import androidx.recyclerview.widget.DiffUtil
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericListAdapter
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericViewHolder

class BudgetDebtAdapter(
    itemCallback: DiffUtil.ItemCallback<DebtUi>,
    private val budgetDebtConfirmActionListener: BudgetDebtConfirmActionListener
) : GenericListAdapter<DebtUi>(itemCallback, R.layout.element_budget_debt) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BudgetDebtViewHolder =
        BudgetDebtViewHolder(super.inflate(parent), budgetDebtConfirmActionListener)

    class BudgetDebtViewHolder(
        view: View, private val budgetDebtConfirmActionListener: BudgetDebtConfirmActionListener
    ) : GenericViewHolder<DebtUi>(view) {
        private val person: TextView = view.findViewById(R.id.person)
        private val amount: TextView = view.findViewById(R.id.amount)
        private val date: TextView = view.findViewById(R.id.date)
        private val statusInfo: TextView = view.findViewById(R.id.statusInfo)
        private val confirm: AppCompatButton = view.findViewById(R.id.confirm)
        private val reject: AppCompatButton = view.findViewById(R.id.reject)
        private val dateAmount: Group = view.findViewById(R.id.date_amount_group)
        private val repay: AppCompatButton = view.findViewById(R.id.repay)
        private val delete: AppCompatImageButton = view.findViewById(R.id.delete)
        private val confirmRejectButtons: Group =
            view.findViewById(R.id.confirm_reject_buttons_group)
        private val typedArray: TypedArray =
            view.context.theme.obtainStyledAttributes(R.styleable.ViewStyle)


        override fun bindTo(item: DebtUi) {
            adjustVisibility(item)
            fillInView(item)
            adjustViewColors(item)
        }

        private fun adjustViewColors(item: DebtUi) {
            view.setBackgroundColor(
                typedArray.getColor(
                    when (item.status) {
                        DebtUi.Status.WAITING -> {
                            R.styleable.ViewStyle_waitingDebtBackground
                        }
                        DebtUi.Status.REJECTED -> {
                            R.styleable.ViewStyle_rejectedDebtBackground
                        }
                        DebtUi.Status.CONFIRMED -> {
                            R.styleable.ViewStyle_confirmedDebtBackground
                        }
                        else -> throw(IllegalArgumentException())
                    }, Color.TRANSPARENT
                )
            )
        }

        private fun fillInView(item: DebtUi) {
            person.text = if (item.amount.isBelongToSignInUser) view.context.getString(R.string.you) else item.user.name
            amount.text = item.amount.amount.toRoundedString()
            date.text = if (item.date.time.isBlank()) view.context.getString(R.string.debtToRepay) else item.date.time
            when (item.status) {
                DebtUi.Status.WAITING -> {
                    if (item.amount.isBelongToSignInUser) {
                        statusInfo.text = view.context.getString(R.string.waitingDebtInfo)
                    } else {
                        confirm.setOnClickListener {
                            budgetDebtConfirmActionListener.onDebtConfirmClicked(item)
                        }
                        reject.setOnClickListener {
                            budgetDebtConfirmActionListener.onDebtRejectClicked(item)
                        }
                    }
                }
                DebtUi.Status.REJECTED -> {
                    statusInfo.text =
                        if (item.amount.isBelongToSignInUser) view.context.getString(R.string.rejectedDebtInfoRepayAgain)
                        else view.context.getString(R.string.rejectedDebtInfo)
                    delete.setOnClickListener { budgetDebtConfirmActionListener.onDebtDeleteClicked(item.id) }
                    repay.setOnClickListener { budgetDebtConfirmActionListener.onDebtRepayAgainClicked(item) }
                }

                DebtUi.Status.CONFIRMED -> statusInfo.text =
                    view.context.getString(R.string.confirmDebtInfo)
            }
        }

        private fun adjustVisibility(item: DebtUi) {
            when (item.status) {
                DebtUi.Status.CONFIRMED -> {
                    confirmRejectButtons.visibility = GONE
                    repay.visibility = GONE
                    delete.visibility = GONE
                    dateAmount.visibility = VISIBLE
                    statusInfo.visibility = VISIBLE
                }
                DebtUi.Status.REJECTED -> {
                    repay.visibility = if (item.amount.isBelongToSignInUser) VISIBLE else GONE
                    delete.visibility = VISIBLE
                    confirmRejectButtons.visibility = GONE
                    dateAmount.visibility = VISIBLE
                    statusInfo.visibility = VISIBLE
                }
                DebtUi.Status.WAITING -> {
                    if (item.amount.isBelongToSignInUser) {
                        confirmRejectButtons.visibility = GONE
                        dateAmount.visibility = VISIBLE
                        statusInfo.visibility = VISIBLE
                    } else {
                        confirmRejectButtons.visibility = VISIBLE
                        dateAmount.visibility = VISIBLE
                        statusInfo.visibility = GONE
                    }
                    repay.visibility = GONE
                    delete.visibility = GONE
                }
            }
        }
    }

    interface BudgetDebtConfirmActionListener {
        fun onDebtConfirmClicked(debt: DebtUi)
        fun onDebtRejectClicked(debt: DebtUi)
        fun onDebtDeleteClicked(idDebt: String)
        fun onDebtRepayAgainClicked(debt: DebtUi)
    }
}
