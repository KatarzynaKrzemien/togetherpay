package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending.details

import android.content.Context
import android.content.Intent
import android.content.res.TypedArray
import android.graphics.Color
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.DatePicker
import androidx.appcompat.app.AlertDialog
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsContract
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.togetherpay.BaseActivity
import com.katarzynaiwonakrzemien.togetherpay.R
import kotlinx.android.synthetic.main.activity_spending_details.*
import java.util.*

class SpendingDetailsActivity :
    BaseActivity<SpendingDetailsContract.Presenter, SpendingDetailsContract.Action, SpendingDetailsContract.Event>(),
    SpendingDetailsContract.View {

    override fun consume(event: SpendingDetailsContract.Event) = with(event) {
        when (this) {
            is SpendingDetailsContract.Event.RenderSpending -> renderSpending(spending)
            is SpendingDetailsContract.Event.RenderSelectedBudgetUsers ->
                renderSelectedBudgetUsers(users)
        }
    }

    private lateinit var idBudget: String
    private lateinit var idSpending: String
    private var spending: SpendingUi? = null
    private lateinit var users: Map<String, String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spending_details)
        setupSavedBudgetArguments(savedInstanceState)
        root.transitionName = idSpending
    }

    private fun setupSavedBudgetArguments(savedInstanceState: Bundle?) {
        idSpending =
            if (savedInstanceState?.containsKey(KEY_ID_SPENDING) == true) {
                savedInstanceState.getString(KEY_ID_SPENDING, null)
            } else {
                intent.getStringExtra(KEY_ID_SPENDING)!!
            }
        idBudget =
            if (savedInstanceState?.containsKey(KEY_ID_BUDGET) == true) {
                savedInstanceState.getString(KEY_ID_BUDGET, null)
            } else {
                intent.getStringExtra(KEY_ID_BUDGET)!!
            }
        send(SpendingDetailsContract.Action.FetchSpending(idBudget, idSpending))
    }

    private fun renderSpending(spendingItem: SpendingUi) {
        if (spending != spendingItem) {
            spending = spendingItem
            setupViews()
        }
    }

    private fun renderSelectedBudgetUsers(users: Map<String, String>) {
        this.users = users
        usersSpinner.adapter = ArrayAdapter(this, R.layout.spinner_item, users.keys.toList())
        usersSpinner.setSelection(users.keys.toList().indexOf(spending!!.user.name))
    }

    private fun setupViews() {
        setupUserSpinner()
        setupBackground()
        setupProduct()
        setupDate()
        setupAmount()
        setupButtons()
    }

    private fun setupUserSpinner() {
        send(SpendingDetailsContract.Action.FetchSelectedBudgetUsers(idBudget))
    }

    private fun setupBackground() {
        val typedArray: TypedArray = theme.obtainStyledAttributes(R.styleable.ViewStyle)
        root.setBackgroundColor(
            if (spending!!.amount.isBelongToSignInUser)
                typedArray.getColor(R.styleable.ViewStyle_userSpendingBackground, Color.TRANSPARENT)
            else
                typedArray.getColor(
                    R.styleable.ViewStyle_otherUserSpendingBackground,
                    Color.TRANSPARENT
                )
        )
    }

    private fun setupProduct() {
        productInput.setText(spending!!.product)
    }

    private fun setupDate() {
        var dateText: DateUi? = null
        date.apply {
            text = spending!!.date.time
            setOnClickListener {
                val dialogView = layoutInflater.inflate(R.layout.dialog_date_picker, null)
                val datePicker = dialogView.findViewById<DatePicker>(R.id.datePicker).apply {
                    maxDate = Calendar.getInstance().timeInMillis
                    if (dateText == null)
                        updateDate(spending!!.date.year, spending!!.date.month, spending!!.date.day)
                    else updateDate(dateText!!.year, dateText!!.month, dateText!!.day)
                }

                AlertDialog.Builder(this@SpendingDetailsActivity).apply {
                    setView(dialogView)
                    setIcon(R.drawable.ic_spending_date)
                    setTitle(R.string.pickDate)
                    setPositiveButton(android.R.string.ok) { dialog, _ ->
                        dateText = DateUi(
                            DateUi.createDate(
                                datePicker.dayOfMonth,
                                datePicker.month,
                                datePicker.year
                            )
                        )
                        date.text = dateText!!.time
                        dialog.dismiss()
                    }
                    setNegativeButton(android.R.string.cancel) { dialog, _ ->
                        dialog.dismiss()
                    }
                    create()
                    show()
                }
            }
        }
    }

    private fun setupAmount() {
        amountInput.setText(spending!!.amount.amount.toRoundedString())
    }

    private fun setupButtons() {
        ok.setOnClickListener {
            when {
                productInput.text.isNullOrBlank() -> {
                    productInputLayout.error = getString(R.string.errorNoProductName)
                }
                amountInput.text.isNullOrBlank() -> {
                    amountInputLayout.error = getString(R.string.errorNoAmount)
                    productInputLayout.error = null
                }
                else -> {
                    send(
                        SpendingDetailsContract.Action.UpdateSpending(
                            idBudget,
                            idSpending,
                            users.getValue(usersSpinner.selectedItem.toString()),
                            usersSpinner.selectedItem.toString(),
                            productInput.text.toString(),
                            date.text.toString(),
                            amountInput.text.toString().toDouble()
                        )
                    )
                    onBackPressed()
                }
            }
        }

        cancel.setOnClickListener {
            onBackPressed()
        }

        delete.setOnClickListener {
            AlertDialog.Builder(this).apply {
                setIcon(R.drawable.ic_delete_dark)
                setTitle(R.string.deleteSpending)
                setMessage(R.string.doYouWantDeleteSpending)
                setPositiveButton(R.string.delete) { dialog, _ ->
                    send(SpendingDetailsContract.Action.DeleteSpending(idSpending))
                    dialog.dismiss()
                    onBackPressed()
                    finish()
                }
                setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss() }
                create()
                show()
            }
        }
    }

    companion object {
        const val KEY_ID_SPENDING = "idSpending"
        const val KEY_ID_BUDGET = "idBudget"
        fun getIntent(context: Context, idSpending: String, idBudget: String): Intent =
            Intent(context, SpendingDetailsActivity::class.java).apply {
                putExtra(KEY_ID_SPENDING, idSpending)
                putExtra(KEY_ID_BUDGET, idBudget)
            }
    }
}
