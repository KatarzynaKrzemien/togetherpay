package com.katarzynaiwonakrzemien.togetherpay.presentation.user.debt

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtContract
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.togetherpay.BaseFragment
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericDiffUtilCallback
import com.katarzynaiwonakrzemien.togetherpay.extension.clearFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.extension.requestFocusAndShowKeyboard
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_budget_select.*

class UserDebtFragment :
    BaseFragment<UserDebtContract.Presenter, UserDebtContract.Action, UserDebtContract.Event>(),
    UserDebtContract.View, UserDebtAdapter.UserDebtActionListener {

    private val adapter = UserDebtAdapter(GenericDiffUtilCallback(), this)
    override val layoutRes: Int = R.layout.fragment_user_debt

    override fun consume(event: UserDebtContract.Event) = with(event) {
        when (this) {
            is UserDebtContract.Event.RenderDebts -> renderDebts(debts)
            is UserDebtContract.Event.RenderDebtSum -> renderDebtSum(debt)
            is UserDebtContract.Event.RenderOwingSum -> renderOwingSum(owing)
            is UserDebtContract.Event.RefreshDone -> refreshDone()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        recycler.adapter = adapter
        send(UserDebtContract.Action.FetchDebts)
        swipeRefresh.setOnRefreshListener { send(UserDebtContract.Action.Refresh) }
        send(UserDebtContract.Action.FetchDebtSum)
        send(UserDebtContract.Action.FetchOwingSum)
    }

    private fun renderDebts(debts: List<DebtUi>) {
        adapter.submitList(debts)
    }

    private fun renderDebtSum(debt: Double) {
        myDebt.text = debt.toRoundedString()
    }

    private fun renderOwingSum(owing: Double) {
        oweMe.text = owing.toRoundedString()
    }

    private fun refreshDone() {
        swipeRefresh.isRefreshing = false
    }

    override fun onDebtConfirmClicked(debt: DebtUi) {
        send(UserDebtContract.Action.ConfirmDebt(debt))
    }

    override fun onDebtRejectClicked(debt: DebtUi) {
        send(UserDebtContract.Action.RejectDebt(debt))
    }

    override fun onDebtDeleteClicked(idDebt: String) {
        send(UserDebtContract.Action.DeleteDebt(idDebt))
    }

    override fun onDebtRepayAllClicked(idBudget: String, amount: Double) {
        send(UserDebtContract.Action.AddDebt(idBudget, amount))
    }

    override fun onDebtRepayAgainClicked(debt: DebtUi) {
        send(UserDebtContract.Action.RepayAgainDebt(debt))
    }

    override fun onDebtRepayAmountClicked(idBudget: String, debtAmount: Double) {
        val dialogView = layoutInflater.inflate(R.layout.dialog_pay_back, null)
        val amount = dialogView.findViewById<TextInputEditText>(R.id.amountInput)
            .apply { requestFocusAndShowKeyboard() }
        val amountLayout: TextInputLayout = dialogView.findViewById(R.id.amountInputLayout)

        AlertDialog.Builder(requireContext()).apply {
            setView(dialogView)
            setIcon(R.drawable.ic_repay_debt)
            setTitle(R.string.payBackSomeMoney)
            setPositiveButton(R.string.repay) { _, _ -> }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                amount.clearFocusAndShowKeyboard()
            }
            create()
        }.show().apply {
            getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                when {
                    amount.text.isNullOrBlank() -> {
                        amountLayout.error = getString(R.string.errorNoAmount)
                    }
                    amount.text.toString().toDoubleOrNull()!! > debtAmount -> {
                        amountLayout.error = getString(R.string.yourDebt, debtAmount)
                    }
                    else -> {
                        send(
                            UserDebtContract.Action.AddDebt(
                                amount = amount.text.toString().toDoubleOrNull()!!,
                                idBudget = idBudget
                            )
                        )
                        amount.clearFocusAndShowKeyboard()
                        dismiss()

                    }
                }
            }
        }
    }
}