package com.katarzynaiwonakrzemien.togetherpay.presentation.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavGraph
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetContract
import com.katarzynaiwonakrzemien.togetherpay.App
import com.katarzynaiwonakrzemien.togetherpay.BaseActivity
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.presentation.start.StartActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_selected_budget.*
import javax.inject.Inject

class UserBudgetActivity :
    BaseActivity<UserBudgetContract.Presenter, UserBudgetContract.Action, UserBudgetContract.Event>(),
    HasAndroidInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>

    override fun consume(event: UserBudgetContract.Event) = with(event) {
        when (this) {
            is UserBudgetContract.Event.RenderSignedInUserName -> renderSignedInUserName(name)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_budget)
        setupInfoBar()
        with(findNavController(R.id.navigationHost)) {
            navigationView.setupWithNavController(this)
            toolbar.setupWithNavController(
                this, AppBarConfiguration(getIdNavDestinationSet(this.graph))
            )
            AppBarConfiguration(this.graph)
        }
        setSupportActionBar(toolbar)
    }

    private fun setupInfoBar() {
        today.text = App.date
        setupSignedOut()
        send(UserBudgetContract.Action.FetchSignedInUserName)
    }

    private fun setupSignedOut() {
        signOut.setOnClickListener {
            finishAffinity()
            startActivity(
                StartActivity.getIntent(this,  signOut = true)
            )
        }
    }

    private fun renderSignedInUserName(name: String) {
        signedInUserName.text = name
    }

    private fun getIdNavDestinationSet(navGraph: NavGraph) =
        navGraph.distinct().map { it.id }.toSet()

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    companion object {
        fun getIntent(context: Context): Intent = Intent(context, UserBudgetActivity::class.java)
    }
}
