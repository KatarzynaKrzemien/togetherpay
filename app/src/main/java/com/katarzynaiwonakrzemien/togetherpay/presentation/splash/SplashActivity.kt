package com.katarzynaiwonakrzemien.togetherpay.presentation.splash

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.start.StartActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(StartActivity.getIntent(context = this, signOut = false))
        finish()
    }
}