package com.katarzynaiwonakrzemien.togetherpay.presentation.budget

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavArgument
import androidx.navigation.NavGraph
import androidx.navigation.NavType
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetContract
import com.katarzynaiwonakrzemien.togetherpay.App
import com.katarzynaiwonakrzemien.togetherpay.BaseActivity
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.presentation.start.StartActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_selected_budget.*
import javax.inject.Inject

class SelectedBudgetActivity :
    BaseActivity<SelectedBudgetContract.Presenter, SelectedBudgetContract.Action, SelectedBudgetContract.Event>(),
    HasAndroidInjector {

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Any>
    private lateinit var idBudget: String
    private var nameBudget: String = String()

    override fun consume(event: SelectedBudgetContract.Event) = with(event) {
        when (this) {
            is SelectedBudgetContract.Event.RenderSignedInUserName -> renderSignedInUserName(
                userName
            )
            is SelectedBudgetContract.Event.RenderBudgetName -> renderBudgetName(budgetName)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_budget)
        setupSavedBudgetArguments(savedInstanceState)
        send(SelectedBudgetContract.Action.FetchBudgetName(idBudget))
        setupInfoBar()
        setSupportActionBar(toolbar)
    }

    private fun setupGraphNav(budgetName: String) {
        with(findNavController(R.id.navigationHost)) {
            navigationView.setupWithNavController(this)
            setGraph(R.navigation.selected_budget_navigation, Bundle().apply {
                putString(KEY_ID_BUDGET, idBudget)
            })
            graph.findNode(R.id.budget_fragment)!!.apply {
                addArgument(
                    KEY_ID_BUDGET,
                    NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget)
                        .build()
                )
                label = budgetName
            }
            graph.findNode(R.id.debt_fragment)!!.apply {
                addArgument(
                    KEY_ID_BUDGET,
                    NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget)
                        .build()
                )
                label = getString(R.string.budget_name_debts, budgetName)
            }
            graph.findNode(R.id.details_fragment)!!.apply {
                addArgument(
                    KEY_ID_BUDGET,
                    NavArgument.Builder().setType(NavType.StringType).setDefaultValue(idBudget)
                        .build()
                )
                label = getString(R.string.budgetDetails, budgetName)
            }
            toolbar.setupWithNavController(
                this, AppBarConfiguration(getIdNavDestinationSet(this.graph))
            )
        }
    }

    private fun setupSavedBudgetArguments(savedInstanceState: Bundle?) {
        idBudget =
            if (savedInstanceState?.containsKey(KEY_ID_BUDGET) == true) {
                savedInstanceState.getString(KEY_ID_BUDGET, null)
            } else {
                intent.getStringExtra(KEY_ID_BUDGET)!!
            }
    }

    private fun setupInfoBar() {
        today.text = App.date
        setupSignedOut()
        send(SelectedBudgetContract.Action.FetchSignedInUserName)
    }

    private fun setupSignedOut() {
        signOut.setOnClickListener {
            finishAffinity()
            startActivity(
                StartActivity.getIntent(this,  signOut = true)
            )
        }
    }

    private fun getIdNavDestinationSet(navGraph: NavGraph) =
        navGraph.distinct().map { it.id }.toSet()

    private fun renderSignedInUserName(userName: String) {
        signedInUserName.text = userName
    }

    private fun renderBudgetName(budgetName: String) {
        if (budgetName != nameBudget) {
            nameBudget = budgetName
            setupGraphNav(budgetName)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = fragmentInjector

    companion object {
        const val KEY_ID_BUDGET = "key_id_budget"
        fun getIntent(context: Context, idBudget: String): Intent =
            Intent(context, SelectedBudgetActivity::class.java).apply {
                putExtra(KEY_ID_BUDGET, idBudget)
            }
    }
}