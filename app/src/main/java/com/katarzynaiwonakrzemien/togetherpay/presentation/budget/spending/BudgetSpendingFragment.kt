package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityOptionsCompat
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingContract
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.togetherpay.BaseFragment
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericDiffUtilCallback
import com.katarzynaiwonakrzemien.togetherpay.extension.clearFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.extension.requestFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.SelectedBudgetActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending.details.SpendingDetailsActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_budget.*

class BudgetSpendingFragment :
    BaseFragment<BudgetSpendingContract.Presenter, BudgetSpendingContract.Action, BudgetSpendingContract.Event>(),
    BudgetSpendingContract.View, BudgetSpendingAdapter.BudgetSpendingActionListener {

    private val adapter = BudgetSpendingAdapter(GenericDiffUtilCallback(), this)

    override val layoutRes: Int = R.layout.fragment_budget
    private lateinit var idBudget: String
    override fun consume(event: BudgetSpendingContract.Event) = with(event) {
        when (this) {
            is BudgetSpendingContract.Event.RenderSpending -> renderSpending(spending)
            is BudgetSpendingContract.Event.RenderCurrentDebt -> renderCurrentDebt(debtInfo)
            is BudgetSpendingContract.Event.RenderSignInUserSpendingSum ->
                renderSignInUserSpendingSum(amount)
            is BudgetSpendingContract.Event.RefreshDone -> refreshDone()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        idBudget = arguments?.getString(SelectedBudgetActivity.KEY_ID_BUDGET)
            ?: throw(IllegalArgumentException(getString(R.string.errorNoBudgetId)))
        send(BudgetSpendingContract.Action.FetchSpending(idBudget))
        send(BudgetSpendingContract.Action.FetchSignInUserSpendingSum(idBudget))
        send(BudgetSpendingContract.Action.FetchCurrentDebt(idBudget))
        recycler.adapter = adapter
        fabAdd.setOnClickListener { showAddSpendingDialog() }
        swipeRefresh.setOnRefreshListener { send(BudgetSpendingContract.Action.Refresh) }
    }

    private fun showAddSpendingDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_add_spending, null)
        val product = dialogView.findViewById<TextInputEditText>(R.id.productInput)
            .apply { requestFocusAndShowKeyboard() }
        val amount: TextInputEditText = dialogView.findViewById(R.id.amountInput)
        val amountLayout: TextInputLayout = dialogView.findViewById(R.id.amountInputLayout)
        val productLayout: TextInputLayout = dialogView.findViewById(R.id.productInputLayout)


        AlertDialog.Builder(requireContext()).apply {
            setView(dialogView)
            setIcon(R.drawable.ic_add)
            setTitle(R.string.add_spending)
            setPositiveButton(R.string.add) { _, _ -> }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                product.clearFocusAndShowKeyboard()
            }
            create()
        }.show().apply {
            getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                when {
                    product.text.isNullOrBlank() -> {
                        productLayout.error = getString(R.string.errorNoProductName)
                    }
                    amount.text.isNullOrBlank() -> {
                        amountLayout.error = getString(R.string.errorNoAmount)
                        productLayout.error = null
                    }
                    else -> {
                        send(
                            BudgetSpendingContract.Action.AddSpending(
                                idBudget = idBudget,
                                product = product.text.toString(),
                                amount = amount.text.toString().toDoubleOrNull()!!
                            )
                        )
                        product.clearFocusAndShowKeyboard()
                        dismiss()
                    }
                }
            }
        }
    }

    private fun renderCurrentDebt(debtInfo: AmountInfoUi) {
        if (debtInfo.amount == 0.0) {
            debtTitle.text = getString(R.string.noDebts)
            debt.text = getString(R.string.empty)
        } else {
            debt.text = debtInfo.amount.toRoundedString()
            debtTitle.text =
                if (debtInfo.isBelongToSignInUser) getString(R.string.myDebt)
                else getString(R.string.oweMe)
        }

    }

    private fun renderSignInUserSpendingSum(amount: Double) {
        spending.text = amount.toRoundedString()
    }

    private fun renderSpending(spending: List<SpendingUi>) {
        adapter.submitList(spending)
    }

    override fun onSpendingClicked(spending: SpendingUi, sharedView: View) {
        val intent = SpendingDetailsActivity.getIntent(requireContext(), spending.id, idBudget)
        val options =
            ActivityOptionsCompat.makeSceneTransitionAnimation(activity!!, sharedView, spending.id)
        startActivity(intent, options.toBundle())
    }

    private fun refreshDone() {
        swipeRefresh.isRefreshing = false
    }
}