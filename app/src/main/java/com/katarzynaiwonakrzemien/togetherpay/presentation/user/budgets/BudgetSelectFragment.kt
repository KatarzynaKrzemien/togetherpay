package com.katarzynaiwonakrzemien.togetherpay.presentation.user.budgets

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.contract.user.budgets.BudgetSelectContract
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.togetherpay.BaseFragment
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericDiffUtilCallback
import com.katarzynaiwonakrzemien.togetherpay.extension.clearFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.extension.requestFocusAndShowKeyboard
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.SelectedBudgetActivity
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.fragment_budget_select.*

class BudgetSelectFragment :
    BaseFragment<BudgetSelectContract.Presenter, BudgetSelectContract.Action, BudgetSelectContract.Event>(),
    BudgetSelectContract.View, BudgetSelectAdapter.BudgetActionListener {

    private val adapter = BudgetSelectAdapter(GenericDiffUtilCallback(), this)
    private var users: List<UserUi> = listOf()
    override val layoutRes: Int = R.layout.fragment_budget_select

    override fun consume(event: BudgetSelectContract.Event) = with(event) {
        when (this) {
            is BudgetSelectContract.Event.RenderBudgets -> renderBudgets(budgets)
            is BudgetSelectContract.Event.RenderUsers -> renderUsers(users)
            is BudgetSelectContract.Event.RenderDebtSum -> renderDebtSum(amount)
            is BudgetSelectContract.Event.RenderOwingSum -> renderOwingSum(amount)
            is BudgetSelectContract.Event.RenderSignInUserSpendingSum ->
                renderSignInUserSpendingSum(amount)
            is BudgetSelectContract.Event.RefreshDone -> refreshDone()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        send(BudgetSelectContract.Action.FetchBudgets)
        send(BudgetSelectContract.Action.FetchDebtSum)
        send(BudgetSelectContract.Action.FetchOwingSum)
        send(BudgetSelectContract.Action.FetchSignInUserSpendingSum)
        send(BudgetSelectContract.Action.FetchUsers)
        recycler.adapter = adapter
        fabAdd.setOnClickListener { showAddBudgetDialog() }
        swipeRefresh.setOnRefreshListener { send(BudgetSelectContract.Action.Refresh) }
    }

    private fun showAddBudgetDialog() {
        val dialogView = layoutInflater.inflate(R.layout.dialog_add_budget, null)
        val name = dialogView.findViewById<TextInputEditText>(R.id.name)
            .apply { requestFocusAndShowKeyboard() }
        val nameLayout: TextInputLayout = dialogView.findViewById(R.id.nameInputLayout)
        val adapter: ArrayAdapter<String> =
            ArrayAdapter(
                requireContext(),
                android.R.layout.select_dialog_item,
                users.map { it.name })
        var otherUserUi: UserUi? = null
        val otherUser =
            dialogView.findViewById<AppCompatAutoCompleteTextView>(R.id.otherUser)
                .apply {
                    setOnItemClickListener { adapterView, _, position, _ ->
                        otherUserUi =
                            users.find { it.name == adapterView.getItemAtPosition(position) }
                    }
                    setAdapter(adapter)
                }

        AlertDialog.Builder(requireContext()).apply {
            setView(dialogView)
            setIcon(R.drawable.ic_add)
            setTitle(R.string.add_budget)
            setPositiveButton(R.string.add) { _, _ -> }
            setNegativeButton(android.R.string.cancel) { _, _ ->
                name.clearFocusAndShowKeyboard()
            }
            create()
        }.show().apply {
            getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                when {
                    name.text.isNullOrBlank() -> {
                        nameLayout.error = getString(R.string.errorNoBudgetName)
                    }
                    otherUserUi == null -> {
                        otherUser.error = getString(R.string.errorNoOtherUser)
                        nameLayout.error = null
                    }
                    else -> {
                        send(
                            BudgetSelectContract.Action.AddBudget(
                                name.text.toString(),
                                otherUserUi!!
                            )
                        )
                        name.clearFocusAndShowKeyboard()
                        dismiss()
                    }
                }
            }
        }
    }

    override fun onBudgetClicked(budget: BudgetUi) {
        startActivity(SelectedBudgetActivity.getIntent(requireContext(), budget.id))
    }

    private fun renderOwingSum(amount: Double) {
        oweMe.text = amount.toRoundedString()
    }

    private fun renderDebtSum(amount: Double) {
        myDebt.text = amount.toRoundedString()
    }

    private fun renderSignInUserSpendingSum(amount: Double) {
        spending.text = amount.toRoundedString()
    }

    private fun renderBudgets(budgets: List<BudgetUi>) {
        adapter.submitList(budgets)
    }

    private fun renderUsers(users: List<UserUi>) {
        this.users = users
    }

    private fun refreshDone() {
        swipeRefresh.isRefreshing = false
    }
}