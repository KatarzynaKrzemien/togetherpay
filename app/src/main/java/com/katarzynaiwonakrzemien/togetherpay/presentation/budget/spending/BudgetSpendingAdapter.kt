package com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending

import android.content.res.TypedArray
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DiffUtil
import com.katarzynaiwonakrzemien.core.extension.toRoundedString
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.togetherpay.R
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericListAdapter
import com.katarzynaiwonakrzemien.togetherpay.adapter.GenericViewHolder

class BudgetSpendingAdapter(
    itemCallback: DiffUtil.ItemCallback<SpendingUi>,
    private val budgetSpendingActionListener: BudgetSpendingActionListener
) : GenericListAdapter<SpendingUi>(itemCallback, R.layout.element_spending) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BudgetSpendingViewHolder =
        BudgetSpendingViewHolder(super.inflate(parent), budgetSpendingActionListener)

    class BudgetSpendingViewHolder(
        view: View,
        private val budgetSpendingActionListener: BudgetSpendingActionListener
    ) : GenericViewHolder<SpendingUi>(view) {
        private val person: TextView = view.findViewById(R.id.person)
        private val amount: TextView = view.findViewById(R.id.amount)
        private val date: TextView = view.findViewById(R.id.date)
        private val product: TextView = view.findViewById(R.id.product)
        private val typedArray: TypedArray = view.context.theme.obtainStyledAttributes(R.styleable.ViewStyle)

        override fun bindTo(item: SpendingUi) {
            ViewCompat.setTransitionName(view, item.id)
            person.text = if (item.amount.isBelongToSignInUser) view.context.getString(R.string.you) else item.user.name
            amount.text = item.amount.amount.toRoundedString()
            date.text = item.date.time
            product.text = item.product
            view.setOnClickListener { budgetSpendingActionListener.onSpendingClicked(item, view) }
            view.setBackgroundColor(
                if (item.amount.isBelongToSignInUser) {
                    typedArray.getColor(
                        R.styleable.ViewStyle_userSpendingBackground, Color.TRANSPARENT
                    )
                } else typedArray.getColor(
                    R.styleable.ViewStyle_otherUserSpendingBackground, Color.TRANSPARENT
                )
            )
        }
    }

    interface BudgetSpendingActionListener {
        fun onSpendingClicked(spending: SpendingUi, sharedView: View)
    }
}
