package com.katarzynaiwonakrzemien.togetherpay.di.module.activity

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetContract
import com.katarzynaiwonakrzemien.domain.contract.user.UserBudgetPresenter
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.UserBudgetScope
import dagger.Module
import dagger.Provides

@Module
class UserBudgetModule {
    @Provides
    @UserBudgetScope
    fun provideUserBudgetPresenter(
        schedulers: RxSchedulers,
        signedInUser: SignedInUser
    ): UserBudgetContract.Presenter = UserBudgetPresenter(schedulers, signedInUser.user)
}