package com.katarzynaiwonakrzemien.togetherpay.di.module.fragment

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingContract
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.BudgetSpendingPresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetScope
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingTransformer
import dagger.Module
import dagger.Provides

@Module
class BudgetModule {
    @Provides
    @BudgetScope
    fun provideBudgetPresenter(
        schedulers: RxSchedulers, repository: RepositoryManager, signedInUser: SignedInUser,
        @SpendingTransformer spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>,
        @BudgetTransformer budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>
    ): BudgetSpendingContract.Presenter =
        BudgetSpendingPresenter(
            schedulers,
            repository,
            signedInUser.user,
            spendingItemToUiTransformer,
            budgetItemToUiTransformer
        )
}