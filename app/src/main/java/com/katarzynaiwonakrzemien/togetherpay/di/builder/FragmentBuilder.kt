package com.katarzynaiwonakrzemien.togetherpay.di.builder

import com.katarzynaiwonakrzemien.togetherpay.di.*
import com.katarzynaiwonakrzemien.togetherpay.di.module.fragment.*
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.debt.BudgetDebtFragment
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.details.BudgetDetailsFragment
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending.BudgetSpendingFragment
import com.katarzynaiwonakrzemien.togetherpay.presentation.user.budgets.BudgetSelectFragment
import com.katarzynaiwonakrzemien.togetherpay.presentation.user.debt.UserDebtFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [UserDebtModule::class])
    @UserDebtScope
    abstract fun userDebtFragment(): UserDebtFragment

    @ContributesAndroidInjector(modules = [BudgetDebtModule::class])
    @BudgetDebtScope
    abstract fun budgetDebtFragment(): BudgetDebtFragment

    @ContributesAndroidInjector(modules = [BudgetSelectModule::class])
    @BudgetSelectScope
    abstract fun budgetSelectFragment(): BudgetSelectFragment

    @ContributesAndroidInjector(modules = [BudgetModule::class])
    @BudgetScope
    abstract fun budgetFragment(): BudgetSpendingFragment

    @ContributesAndroidInjector(modules = [BudgetDetailsModule::class])
    @BudgetDetailsScope
    abstract fun budgetDetailsFragment(): BudgetDetailsFragment
}