package com.katarzynaiwonakrzemien.togetherpay.di.module.fragment

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtContract
import com.katarzynaiwonakrzemien.domain.contract.budget.debt.BudgetDebtPresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetDebtScope
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.DebtTransformer
import dagger.Module
import dagger.Provides

@Module
class BudgetDebtModule {
    @Provides
    @BudgetDebtScope
    fun provideBudgetDebtPresenter(
        schedulers: RxSchedulers, repository: RepositoryManager, signedInUser: SignedInUser,
        @DebtTransformer debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>,
        @BudgetTransformer budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
        @DebtTransformer debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
    ): BudgetDebtContract.Presenter = BudgetDebtPresenter(
        schedulers,
        repository,
        signedInUser.user,
        debtItemToUiTransformer,
        budgetItemToUiTransformer,
        debtUiToItemTransformer
    )
}