package com.katarzynaiwonakrzemien.togetherpay.di.module.fragment

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsContract
import com.katarzynaiwonakrzemien.domain.contract.budget.details.BudgetDetailsPresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetDetailsScope
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import dagger.Module
import dagger.Provides

@Module
class BudgetDetailsModule {
    @Provides
    @BudgetDetailsScope
    fun provideBudgetDetailsPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        signedInUser: SignedInUser,
        @BudgetTransformer budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
        @BudgetTransformer budgetUiToItemTransformer: UiToItemTransformer<BudgetUi, Budget>
    ): BudgetDetailsContract.Presenter = BudgetDetailsPresenter(
        schedulers,
        repository,
        signedInUser.user,
        budgetItemToUiTransformer,
        budgetUiToItemTransformer
    )
}