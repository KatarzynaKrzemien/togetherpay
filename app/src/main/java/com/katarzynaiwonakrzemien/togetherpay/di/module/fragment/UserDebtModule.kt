package com.katarzynaiwonakrzemien.togetherpay.di.module.fragment

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtContract
import com.katarzynaiwonakrzemien.domain.contract.user.debt.UserDebtPresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.DebtTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.UserDebtScope
import dagger.Module
import dagger.Provides

@Module
class UserDebtModule {
    @Provides
    @UserDebtScope
    fun provideUserDebtPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        idSignedUser: SignedInUser,
        @DebtTransformer debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>,
        @BudgetTransformer budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
        @DebtTransformer debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
    ): UserDebtContract.Presenter =
        UserDebtPresenter(
            schedulers,
            repository,
            idSignedUser.user,
            debtItemToUiTransformer,
            budgetItemToUiTransformer,
            debtUiToItemTransformer
        )
}