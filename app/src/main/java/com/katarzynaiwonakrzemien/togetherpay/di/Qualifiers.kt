package com.katarzynaiwonakrzemien.togetherpay.di

import javax.inject.Qualifier

@Qualifier
annotation class UserTransformer

@Qualifier
annotation class BudgetTransformer

@Qualifier
annotation class DebtTransformer

@Qualifier
annotation class SpendingTransformer