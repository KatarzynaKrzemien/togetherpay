package com.katarzynaiwonakrzemien.togetherpay.di.module

import com.katarzynaiwonakrzemien.core.repository.Repository
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.repository.FirebaseRepository
import com.katarzynaiwonakrzemien.togetherpay.repository.LocalRepositoryManager
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object RepositoryModule {
    @Provides
    @Singleton
    fun provideFirebaseRepository(signedInUser: SignedInUser): Repository =
        FirebaseRepository(FirebaseFirestore.getInstance().apply {
            firestoreSettings =
                FirebaseFirestoreSettings.Builder().setPersistenceEnabled(false).build()
        }, signedInUser)

    @Provides
    @Singleton
    fun provideRepositoryManager(
        repository: Repository
    ): RepositoryManager = LocalRepositoryManager(repository)
}
