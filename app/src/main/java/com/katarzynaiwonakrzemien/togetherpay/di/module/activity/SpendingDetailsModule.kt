package com.katarzynaiwonakrzemien.togetherpay.di.module.activity

import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsContract
import com.katarzynaiwonakrzemien.domain.contract.budget.spending.details.SpendingDetailsPresenter
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingDetailsScope
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingTransformer
import dagger.Module
import dagger.Provides

@Module
class SpendingDetailsModule {

    @Provides
    @SpendingDetailsScope
    fun provideSpendingDetailsPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        @SpendingTransformer spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>
    ): SpendingDetailsContract.Presenter =
    SpendingDetailsPresenter(schedulers, repository, spendingItemToUiTransformer)
}