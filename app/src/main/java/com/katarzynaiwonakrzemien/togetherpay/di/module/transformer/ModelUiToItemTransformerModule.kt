package com.katarzynaiwonakrzemien.togetherpay.di.module.transformer

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import com.katarzynaiwonakrzemien.domain.transformer.budget.BudgetUiToItemTransformer
import com.katarzynaiwonakrzemien.domain.transformer.debt.DebtUiToItemTransformer
import com.katarzynaiwonakrzemien.domain.transformer.spending.SpendingUiToItemTransformer
import com.katarzynaiwonakrzemien.domain.transformer.user.UserUiToItemTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.DebtTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.UserTransformer
import dagger.Module
import dagger.Provides

@Module
class ModelUiToItemTransformerModule {
    @UserTransformer
    @Provides
    fun provideUserUiToItemTransformer(): UiToItemTransformer<UserUi, User> =
        UserUiToItemTransformer()

    @DebtTransformer
    @Provides
    fun provideDebtUiToItemTransformer(): UiToItemTransformer<DebtUi, Debt> =
        DebtUiToItemTransformer()

    @SpendingTransformer
    @Provides
    fun provideSpendingUiToItemTransformer(): UiToItemTransformer<SpendingUi, Spending> =
        SpendingUiToItemTransformer()

    @BudgetTransformer
    @Provides
    fun provideBudgetUiToItemTransformer(
        signedInUser: SignedInUser,
        @SpendingTransformer spendingUiToItemTransformer: UiToItemTransformer<SpendingUi, Spending>,
        @DebtTransformer debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
    ): UiToItemTransformer<BudgetUi, Budget> =
        BudgetUiToItemTransformer(
            signedInUser.user,
            spendingUiToItemTransformer,
            debtUiToItemTransformer
        )
}