package com.katarzynaiwonakrzemien.togetherpay.di

import android.content.Context
import com.katarzynaiwonakrzemien.togetherpay.App
import com.katarzynaiwonakrzemien.togetherpay.di.builder.ActivityBuilder
import com.katarzynaiwonakrzemien.togetherpay.di.builder.FragmentBuilder
import com.katarzynaiwonakrzemien.togetherpay.di.module.IoModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.RepositoryModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.SignedInUserModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.transformer.ModelItemToUiTransformerModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.transformer.ModelUiToItemTransformerModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityBuilder::class,
        FragmentBuilder::class,
        IoModule::class,
        RepositoryModule::class,
        SignedInUserModule::class,
        ModelItemToUiTransformerModule::class,
        ModelUiToItemTransformerModule::class
    ]
)
@Singleton
interface AppComponent {
    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: Context): Builder

        fun build(): AppComponent
    }
}
