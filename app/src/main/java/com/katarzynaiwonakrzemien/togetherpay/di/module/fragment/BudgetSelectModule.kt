package com.katarzynaiwonakrzemien.togetherpay.di.module.fragment

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.user.budgets.BudgetSelectContract
import com.katarzynaiwonakrzemien.domain.contract.user.budgets.BudgetSelectPresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetSelectScope
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.UserTransformer
import dagger.Module
import dagger.Provides

@Module
class BudgetSelectModule {
    @Provides
    @BudgetSelectScope
    fun provideBudgetSelectPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        idSignedUser: SignedInUser,
        @BudgetTransformer budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
        @UserTransformer userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
    ): BudgetSelectContract.Presenter = BudgetSelectPresenter(
        schedulers,
        repository,
        idSignedUser.user,
        budgetItemToUiTransformer,
        userItemToUiTransformer
    )
}