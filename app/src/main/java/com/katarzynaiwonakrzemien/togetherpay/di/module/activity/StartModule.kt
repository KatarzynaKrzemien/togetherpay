package com.katarzynaiwonakrzemien.togetherpay.di.module.activity

import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.start.StartContract
import com.katarzynaiwonakrzemien.domain.contract.start.StartPresenter
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.StartScope
import dagger.Module
import dagger.Provides

@Module
class StartModule {
    @Provides
    @StartScope
    fun provideStartPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        signedInUser: SignedInUser
    ): StartContract.Presenter = StartPresenter(schedulers, repository, signedInUser)
}