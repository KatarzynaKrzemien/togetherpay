package com.katarzynaiwonakrzemien.togetherpay.di.module

import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.user.FirebaseSignedInUser
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object SignedInUserModule {
    @Provides
    @Singleton
    fun provideFirebaseSignedInUser(): SignedInUser = FirebaseSignedInUser()
}