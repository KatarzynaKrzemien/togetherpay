package com.katarzynaiwonakrzemien.togetherpay.di.builder

import com.katarzynaiwonakrzemien.togetherpay.di.SelectedBudgetScope
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingDetailsScope
import com.katarzynaiwonakrzemien.togetherpay.di.StartScope
import com.katarzynaiwonakrzemien.togetherpay.di.UserBudgetScope
import com.katarzynaiwonakrzemien.togetherpay.di.module.activity.SelectedBudgetModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.activity.SpendingDetailsModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.activity.StartModule
import com.katarzynaiwonakrzemien.togetherpay.di.module.activity.UserBudgetModule
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.SelectedBudgetActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.budget.spending.details.SpendingDetailsActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.start.StartActivity
import com.katarzynaiwonakrzemien.togetherpay.presentation.user.UserBudgetActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [SelectedBudgetModule::class])
    @SelectedBudgetScope
    abstract fun selectedBudgetActivity(): SelectedBudgetActivity

    @ContributesAndroidInjector(modules = [UserBudgetModule::class])
    @UserBudgetScope
    abstract fun userBudgetActivity(): UserBudgetActivity

    @ContributesAndroidInjector(modules = [StartModule::class])
    @StartScope
    abstract fun startActivity(): StartActivity

    @ContributesAndroidInjector(modules = [SpendingDetailsModule::class])
    @SpendingDetailsScope
    abstract fun spendingDetailsActivity(): SpendingDetailsActivity
}