package com.katarzynaiwonakrzemien.togetherpay.di

import javax.inject.Scope

@Retention
@Scope
annotation class BudgetScope

@Retention
@Scope
annotation class SelectedBudgetScope

@Retention
@Scope
annotation class BudgetSelectScope

@Retention
@Scope
annotation class UserDebtScope

@Retention
@Scope
annotation class BudgetDebtScope

@Retention
@Scope
annotation class StartScope

@Retention
@Scope
annotation class UserBudgetScope

@Retention
@Scope
annotation class SpendingDetailsScope

@Retention
@Scope
annotation class BudgetDetailsScope