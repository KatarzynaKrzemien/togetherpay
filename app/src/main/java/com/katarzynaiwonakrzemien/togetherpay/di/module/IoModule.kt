package com.katarzynaiwonakrzemien.togetherpay.di.module

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.togetherpay.rx.AndroidSchedulers
import dagger.Module
import dagger.Provides

@Module
class IoModule {
    @Provides
    fun provideRxSchedulers(): RxSchedulers = AndroidSchedulers()
}