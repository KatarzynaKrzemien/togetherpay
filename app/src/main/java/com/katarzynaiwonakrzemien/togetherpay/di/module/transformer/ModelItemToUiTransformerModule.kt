package com.katarzynaiwonakrzemien.togetherpay.di.module.transformer

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.budget.BudgetItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.debt.DebtItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.spending.SpendingItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.user.UserItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.BudgetTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.DebtTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.SpendingTransformer
import com.katarzynaiwonakrzemien.togetherpay.di.UserTransformer
import dagger.Module
import dagger.Provides

@Module
class ModelItemToUiTransformerModule {
    @UserTransformer
    @Provides
    fun provideUserItemToUiTransformer(): ItemToUiTransformer<User, UserUi> =
        UserItemToUiTransformer()

    @BudgetTransformer
    @Provides
    fun provideBudgetItemToUiTransformer(
        signedInUser: SignedInUser,
        @UserTransformer userItemToUiTransformer: ItemToUiTransformer<User, UserUi>,
        @SpendingTransformer spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>,
        @DebtTransformer debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>
    ): ItemToUiTransformer<Budget, BudgetUi> =
        BudgetItemToUiTransformer(
            signedInUser.user,
            userItemToUiTransformer,
            spendingItemToUiTransformer,
            debtItemToUiTransformer
        )

    @DebtTransformer
    @Provides
    fun provideDebtItemToUiTransformer(
        signedInUser: SignedInUser,
        @UserTransformer userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
    ): ItemToUiTransformer<Debt, DebtUi> =
        DebtItemToUiTransformer(signedInUser.user, userItemToUiTransformer)

    @SpendingTransformer
    @Provides
    fun provideSpendingItemToUiTransformer(
        signedInUser: SignedInUser,
        @UserTransformer userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
    ): ItemToUiTransformer<Spending, SpendingUi> =
        SpendingItemToUiTransformer(signedInUser.user, userItemToUiTransformer)
}