package com.katarzynaiwonakrzemien.togetherpay.di.module.activity

import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetContract
import com.katarzynaiwonakrzemien.domain.contract.budget.SelectedBudgetPresenter
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.katarzynaiwonakrzemien.togetherpay.di.SelectedBudgetScope
import dagger.Module
import dagger.Provides

@Module
class SelectedBudgetModule {
    @Provides
    @SelectedBudgetScope
    fun provideSelectedBudgetPresenter(
        schedulers: RxSchedulers,
        repository: RepositoryManager,
        signedUser: SignedInUser
    ): SelectedBudgetContract.Presenter =
        SelectedBudgetPresenter(schedulers, repository, signedUser.user)
}