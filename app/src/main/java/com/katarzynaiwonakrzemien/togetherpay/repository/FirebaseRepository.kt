package com.katarzynaiwonakrzemien.togetherpay.repository

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.Repository
import com.katarzynaiwonakrzemien.domain.user.SignedInUser
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function3
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.TimeUnit

class FirebaseRepository(
    private val firebaseFirestore: FirebaseFirestore,
    private val signedInUser: SignedInUser
) : Repository {

    private val intervalPeriod = 60L
    private val intervalStart = 0L
    private val USERS = "users"
    private val BUDGETS = "budgets"
    private val SPENDING = "spending"
    private val REPAID_DEBTS = "repaidDebts"

    private val usersSubject: BehaviorSubject<List<User>> = BehaviorSubject.create()
    private val users = Observable.interval(intervalStart, intervalPeriod, TimeUnit.SECONDS)
        .flatMap {
            firebaseFirestore.collection(USERS)
                .get()
                .toSingle().map {
                    it.documents.map { document -> document.toObject(User::class.java) }
                        .filterIsInstance(User::class.java)
                }
                .toObservable()
        }

    private val budgetsSubject: BehaviorSubject<List<Budget>> = BehaviorSubject.create()
    private val budgets =
        Observable.interval(intervalStart, intervalPeriod, TimeUnit.SECONDS).flatMap {
            Observable.zip(
                firebaseFirestore.collection(BUDGETS)
                    .get()
                    .toSingle().map {
                        it.documents.map { document -> document.toObject(Budget::class.java) }
                            .filterIsInstance(Budget::class.java)
                    }
                    .toObservable(),
                firebaseFirestore.collection(SPENDING)
                    .get()
                    .toSingle().map {
                        it.documents.map { document -> document.toObject(Spending::class.java) }
                            .filterIsInstance(Spending::class.java)
                    }.toObservable(),
                firebaseFirestore.collection(REPAID_DEBTS)
                    .get()
                    .toSingle().map {
                        it.documents.map { document -> document.toObject(Debt::class.java) }
                            .filterIsInstance(Debt::class.java)
                    }.toObservable(),
                Function3<List<Budget>, List<Spending>, List<Debt>, List<Budget>>
                { budgets, spending, debts ->
                    budgets.filter { it.firstUserId == signedInUser.user.id || it.secondUserId == signedInUser.user.id }
                        .map { budget ->
                            budget.copy(
                                spending = spending.filter { it.idBudget == budget.id },
                                repaidDebts = debts.filter { it.idBudget == budget.id })
                        }
                })
        }

    override fun budgets(): Observable<List<Budget>> {
        budgets.subscribe { budgetsSubject.onNext(it) }
        return budgetsSubject
    }

    override fun users(): Observable<List<User>> {
        users.subscribe { usersSubject.onNext(it) }
        return usersSubject
    }

    override fun addUserIfNotExist(user: User) {
        firebaseFirestore
            .collection(USERS).document(user.id)
            .set(user.toHashMap(), SetOptions.merge())
    }

    override fun addBudget(budget: Budget) {
        firebaseFirestore
            .collection(BUDGETS).document(budget.id)
            .set(budget.toHashMap(), SetOptions.merge())
        refreshBudgets()
    }

    override fun addSpending(spending: Spending) {
        firebaseFirestore
            .collection(SPENDING).document(spending.id)
            .set(spending.toHashMap(), SetOptions.merge())
        refreshBudgets()
    }

    override fun addDebt(debt: Debt) {
        firebaseFirestore
            .collection(REPAID_DEBTS).document(debt.id)
            .set(debt.toHashMap(), SetOptions.merge())
        refreshBudgets()
    }

    override fun deleteBudget(budget: Budget) {
        budget.spending.forEach { firebaseFirestore.collection(SPENDING).document(it.id).delete() }
        budget.repaidDebts.forEach {
            firebaseFirestore.collection(REPAID_DEBTS).document(it.id).delete()
        }
        firebaseFirestore.collection(BUDGETS).document(budget.id).delete()
        refreshBudgets()
    }

    override fun deleteSpending(id: String) {
        firebaseFirestore.collection(SPENDING).document(id).delete()
        refreshBudgets()
    }

    override fun deleteDebt(id: String) {
        firebaseFirestore.collection(REPAID_DEBTS).document(id).delete()
        refreshBudgets()
    }

    override fun updateDebt(debt: Debt) {
        firebaseFirestore
            .collection(REPAID_DEBTS)
            .document(debt.id)
            .set(debt.toHashMap(), SetOptions.merge())
        refreshBudgets()
    }

    override fun updateBudget(budget: Budget) {
        firebaseFirestore
            .collection(BUDGETS)
            .document(budget.id).set(
            budget.toHashMap(),
            SetOptions.merge()
        )
        refreshBudgets()
    }

    override fun updateSpending(spending: Spending) {
        firebaseFirestore
            .collection(SPENDING)
            .document(spending.id).set(
            spending.toHashMap(),
            SetOptions.merge()
        )
        refreshBudgets()
    }

    override fun refreshBudgets() {
        budgets.subscribe { budgetsSubject.onNext(it) }
    }

    override fun refreshUsers() {
        users.subscribe { usersSubject.onNext(it) }
    }

    private fun <T> Task<T>.toSingle(): Single<T> = Single.create { emitter ->
        addOnSuccessListener(emitter::onSuccess)
        addOnFailureListener(emitter::onError)
    }
}