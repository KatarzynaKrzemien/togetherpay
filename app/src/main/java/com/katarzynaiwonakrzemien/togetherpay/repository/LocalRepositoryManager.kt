package com.katarzynaiwonakrzemien.togetherpay.repository

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.Repository
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import io.reactivex.Observable

class LocalRepositoryManager(private val repository: Repository) : RepositoryManager {

    private var users: Observable<List<User>>? = null
    private var budgets: Observable<List<Budget>>? = null

    override fun cleanData() {
        users = null
        budgets = null
    }

    override fun budgets(): Observable<List<Budget>> {
        if (budgets == null) budgets = repository.budgets()
        return budgets!!
    }

    override fun users(): Observable<List<User>> {
        if (users == null) users = repository.users()
        return users!!
    }

    override fun addUserIfNotExist(user: User) {
        repository.addUserIfNotExist(user)
    }

    override fun addBudget(budget: Budget) {
        repository.addBudget(budget)
    }

    override fun addSpending(spending: Spending) {
        repository.addSpending(spending)
    }

    override fun addDebt(debt: Debt) {
        repository.addDebt(debt)
    }

    override fun deleteBudget(budget: Budget) {
        repository.deleteBudget(budget)
    }

    override fun deleteSpending(id: String) {
        repository.deleteSpending(id)
    }

    override fun deleteDebt(id: String) {
        repository.deleteDebt(id)
    }

    override fun updateDebt(debt: Debt) {
        repository.updateDebt(debt)
    }

    override fun updateBudget(budget: Budget) {
        repository.updateBudget(budget)
    }

    override fun updateSpending(spending: Spending) {
        repository.updateSpending(spending)
    }

    override fun refreshBudgets() {
        repository.refreshBudgets()
    }

    override fun refreshUsers() {
        repository.refreshUsers()
    }
}