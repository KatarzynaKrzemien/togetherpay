package com.katarzynaiwonakrzemien.togetherpay.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.katarzynaiwonakrzemien.domain.model.ItemUi

abstract class GenericListAdapter<T>(
    itemCallback: DiffUtil.ItemCallback<T>,
    @LayoutRes private val layoutRes: Int
) : ListAdapter<T, GenericViewHolder<T>>(itemCallback) {

    open fun inflate(parent: ViewGroup): View =
        LayoutInflater.from(parent.context).inflate(layoutRes, parent, false)

    override fun onBindViewHolder(holder: GenericViewHolder<T>, position: Int) =
        holder.bindTo(getItem(position))

    override fun getItemViewType(position: Int): Int = layoutRes
}

class GenericDiffUtilCallback<T : ItemUi> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean = oldItem.equals(newItem)
}

abstract class GenericViewHolder<T>(protected val view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bindTo(item: T)
}

