package com.katarzynaiwonakrzemien.togetherpay.user

import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.user.SignedInUser

class FirebaseSignedInUser : SignedInUser {

    var state = Signed.OUT
    override var user: User = User()
        get() = if (state == Signed.IN) field else throw IllegalAccessError("User signed out")
        set(value) {
            field = value
            state = Signed.IN
        }

    override fun signOut() {
        state = Signed.OUT
    }

    override fun signIn(user: User) {
        state = Signed.IN
        this.user = user
    }

    companion object {
        enum class Signed {
            IN,
            OUT
        }
    }
}
