package com.katarzynaiwonakrzemien.togetherpay.log

import com.katarzynaiwonakrzemien.core.log.Log
import android.util.Log as AndroidLogger

class AndroidLog : Log {
    override fun d(tag: String, message: String) {
        AndroidLogger.d(tag, message)
    }

    override fun w(tag: String, message: String, throwable: Throwable?) {
        AndroidLogger.w(tag, message, throwable)
    }

    override fun e(tag: String, message: String, throwable: Throwable?) {
        AndroidLogger.e(tag, message, throwable)
    }

    override fun i(tag: String, message: String) {
        AndroidLogger.i(tag, message)
    }

    override fun wtf(tag: String, message: String) {
        AndroidLogger.wtf(tag, message)
    }
}