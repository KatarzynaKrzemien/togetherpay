package com.katarzynaiwonakrzemien.togetherpay

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BaseContract
import dagger.android.AndroidInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

abstract class BaseActivity<P : BaseContract.Presenter<E, A>, A : BaseContract.Action, E : BaseContract.Event> :
    AppCompatActivity(), BaseContract.View {

    @Inject
    lateinit var presenter: P

    @Inject
    lateinit var schedulers: RxSchedulers

    private val disposables = CompositeDisposable()

    protected val actionSubject: Subject<A> = PublishSubject.create()

    abstract fun consume(event: E)

    fun send(action: A) = actionSubject.onNext(action)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        presenter.attach(actionSubject)
        presenter.subscribe()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = ::consume)
            .let(::addDisposable)
        super.onCreate(savedInstanceState)
    }

    fun addDisposable(disposable: Disposable) = disposables.add(disposable)

    override fun onDestroy() {
        presenter.dispose()
        disposables.clear()
        super.onDestroy()
    }
}