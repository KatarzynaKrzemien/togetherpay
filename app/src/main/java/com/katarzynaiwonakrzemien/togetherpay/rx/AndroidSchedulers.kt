package com.katarzynaiwonakrzemien.togetherpay.rx

import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AndroidSchedulers : RxSchedulers {
    override val background: Scheduler = Schedulers.io()
    override val ui: Scheduler = AndroidSchedulers.mainThread()
}
