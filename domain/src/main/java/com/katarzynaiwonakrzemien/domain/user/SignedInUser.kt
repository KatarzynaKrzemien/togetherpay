package com.katarzynaiwonakrzemien.domain.user

import com.katarzynaiwonakrzemien.core.model.User

interface SignedInUser {
    var user: User
    fun signOut()
    fun signIn(user: User)
}