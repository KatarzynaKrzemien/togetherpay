package com.katarzynaiwonakrzemien.domain.contract.budget.debt

import com.katarzynaiwonakrzemien.domain.BaseContract
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import java.util.*

class BudgetDebtContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        data class FetchRepaidDebts(val idBudget: String) : Action()
        data class FetchCurrentDebt(val idBudget: String) : Action()
        object Refresh : Action()
        data class DeleteDebt(val idDebt: String) : Action()
        data class RejectDebt(
            val debt: DebtUi,
            val status: DebtUi.Status = DebtUi.Status.REJECTED,
            val updateDate: DateUi = DateUi(
                DateUi.createDate(
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.YEAR)
                )
            )
        ) : Action()
        data class ConfirmDebt(
            val debt: DebtUi,
            val status: DebtUi.Status = DebtUi.Status.CONFIRMED,
            val updateDate: DateUi = DateUi(
                DateUi.createDate(
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.YEAR)
                )
            )
        ) : Action()
        data class RepayAgainDebt(
            val debt: DebtUi,
            val status: DebtUi.Status = DebtUi.Status.WAITING,
            val updateDate: DateUi = DateUi(
                DateUi.createDate(
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.YEAR)
                )
            )
        ) : Action()
        data class AddDebt(
            val idBudget: String,
            val amount: Double,
            val date: String = DateUi.createDate(
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR)
            )
        ) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderRepaidDebts(val debts: List<DebtUi>) : Event()
        data class RenderCurrentDebt(val debtInfo: AmountInfoUi?) : Event()
        object RefreshDone : Event()
    }
}
