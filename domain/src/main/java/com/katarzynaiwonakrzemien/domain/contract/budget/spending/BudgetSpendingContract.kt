package com.katarzynaiwonakrzemien.domain.contract.budget.spending

import com.katarzynaiwonakrzemien.domain.BaseContract
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import java.util.*

class BudgetSpendingContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        data class FetchSpending(val idBudget: String) : Action()
        data class FetchCurrentDebt(val idBudget: String) : Action()
        data class FetchSignInUserSpendingSum(val idBudget: String) : Action()
        object Refresh : Action()
        data class AddSpending(
            val idBudget: String,
            val product: String,
            val amount: Double,
            val date: String = DateUi.createDate(
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR)
            )
        ) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderSpending(val spending: List<SpendingUi>) : Event()
        data class RenderSignInUserSpendingSum(val amount: Double) : Event()
        data class RenderCurrentDebt(val debtInfo: AmountInfoUi) : Event()
        object RefreshDone : Event()
    }
}
