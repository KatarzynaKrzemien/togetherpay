package com.katarzynaiwonakrzemien.domain.contract.budget.details

import com.katarzynaiwonakrzemien.domain.BaseContract
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import java.util.*

class BudgetDetailsContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        data class FetchBudget(val idBudget: String) : Action()
        data class DeleteBudget(val budget: BudgetUi) : Action()
        data class UpdateBudget(val name: String, val budget: BudgetUi) : Action()
        data class ClearBudget(
            val budget: BudgetUi,
            val date: String = DateUi.createDate(
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR)
            )
        ) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderBudget(val budget: BudgetUi) : Event()
    }
}
