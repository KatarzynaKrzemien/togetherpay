package com.katarzynaiwonakrzemien.domain.contract.start

import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.user.SignedInUser

class StartPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: SignedInUser
) : BasePresenter<StartContract.Event, StartContract.Action>(rxSchedulers),
    StartContract.Presenter {

    override fun consume(action: StartContract.Action) = with(action) {
        when (this) {
            is StartContract.Action.AddSignedInUser -> addSignedInUser(id, name)
            is StartContract.Action.SignedOutUser -> signedOutUser()
        }
    }

    private fun addSignedInUser(id: String, name: String) {
        signedInUser.signIn(User(id, name))
        repository.addUserIfNotExist(signedInUser.user)
    }

    private fun signedOutUser() {
        repository.cleanData()
        signedInUser.signOut()
    }
}