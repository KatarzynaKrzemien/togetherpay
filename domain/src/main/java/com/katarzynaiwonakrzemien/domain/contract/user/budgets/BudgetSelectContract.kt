package com.katarzynaiwonakrzemien.domain.contract.user.budgets

import com.katarzynaiwonakrzemien.domain.BaseContract
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.UserUi

class BudgetSelectContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object FetchBudgets : Action()
        object FetchUsers : Action()
        object FetchDebtSum : Action()
        object FetchOwingSum : Action()
        object FetchSignInUserSpendingSum : Action()
        object Refresh : Action()
        data class AddBudget(val name: String, val idOtherUser: UserUi) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderBudgets(val budgets: List<BudgetUi>) : Event()
        data class RenderDebtSum(val amount: Double) : Event()
        data class RenderOwingSum(val amount: Double) : Event()
        data class RenderSignInUserSpendingSum(val amount: Double) : Event()
        data class RenderUsers(val users: List<UserUi>) : Event()
        object RefreshDone : Event()
    }
}
