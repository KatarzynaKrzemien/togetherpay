package com.katarzynaiwonakrzemien.domain.contract.start

import com.katarzynaiwonakrzemien.domain.BaseContract

class StartContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        data class AddSignedInUser(val id: String, val name: String) : Action()
        object SignedOutUser : Action()
    }

    sealed class Event : BaseContract.Event
}
