package com.katarzynaiwonakrzemien.domain.contract.budget.spending

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import io.reactivex.rxkotlin.subscribeBy

class BudgetSpendingPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User,
    private val spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>,
    private val budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>
) : BasePresenter<BudgetSpendingContract.Event, BudgetSpendingContract.Action>(rxSchedulers),
    BudgetSpendingContract.Presenter {

    override fun consume(action: BudgetSpendingContract.Action) = with(action) {
        when (this) {
            is BudgetSpendingContract.Action.FetchSpending -> fetchSpending(idBudget)
            is BudgetSpendingContract.Action.FetchCurrentDebt -> fetchCurrentDebt(idBudget)
            is BudgetSpendingContract.Action.FetchSignInUserSpendingSum ->
                fetchSignInUserSpendingSum(idBudget)
            is BudgetSpendingContract.Action.AddSpending ->
                addSpending(idBudget, product, amount, date)
            is BudgetSpendingContract.Action.Refresh -> refresh()
        }
    }

    private fun fetchSpending(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSpendingContract.Event.RenderSpending(
                        budgets.find { it.id == idBudget }!!
                            .spending
                            .asSequence()
                            .map { spendingItemToUiTransformer.transform(it) }
                            .sortedBy { it.product }
                            .sortedByDescending { it.date.day }
                            .sortedByDescending { it.date.month }
                            .sortedByDescending { it.date.year }
                            .toList())
                )
            },
                onError = { Log.e(TAG, "Failure to fetch spending", it) }
            ).let(::addDisposable)
    }

    private fun fetchCurrentDebt(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSpendingContract.Event.RenderCurrentDebt(
                        budgetItemToUiTransformer.transform(
                            budgets.find { it.id == idBudget }!!
                        ).debt
                    )
                )
                send(BudgetSpendingContract.Event.RefreshDone)
            },
                onError = { Log.e(TAG, "Failure to fetch current debt", it) }
            ).let(::addDisposable)
    }

    private fun fetchSignInUserSpendingSum(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSpendingContract.Event.RenderSignInUserSpendingSum(
                        with(budgets.find { it.id == idBudget }!!) {
                            spending
                                .filter { it.idUser == signedInUser.id }
                                .sumByDouble { it.amount }
                                .plus(
                                    repaidDebts
                                        .filter { it.idUser == signedInUser.id }
                                        .filter { it.status == Debt.CONFIRMED || it.status == Debt.WAITING }
                                        .sumByDouble { it.amount })
                        })
                )
            },
                onError =
                { Log.e(TAG, "Failure to fetch sign in user spending sum", it) }
            ).let(::addDisposable)
    }

    private fun addSpending(idBudget: String, product: String, amount: Double, date: String) {
        repository.addSpending(
            Spending(
                idUser = signedInUser.id,
                idBudget = idBudget,
                nameUser = signedInUser.name,
                product = product,
                amount = amount,
                date = date
            )
        )
    }

    private fun refresh() {
        repository.refreshBudgets()
    }
}