package com.katarzynaiwonakrzemien.domain.contract.budget

import com.katarzynaiwonakrzemien.domain.BaseContract

class SelectedBudgetContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object FetchSignedInUserName : Action()
        data class FetchBudgetName(val idBudget: String) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderSignedInUserName(val userName: String) : Event()
        data class RenderBudgetName(val budgetName: String) : Event()
    }
}
