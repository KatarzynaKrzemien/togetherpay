package com.katarzynaiwonakrzemien.domain.contract.budget.spending.details

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import io.reactivex.rxkotlin.subscribeBy

class SpendingDetailsPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>
) : BasePresenter<SpendingDetailsContract.Event, SpendingDetailsContract.Action>(rxSchedulers),
    SpendingDetailsContract.Presenter {

    override fun consume(action: SpendingDetailsContract.Action) = with(action) {
        when (this) {
            is SpendingDetailsContract.Action.FetchSpending -> fetchSpending(idBudget, idSpending)
            is SpendingDetailsContract.Action.FetchSelectedBudgetUsers ->
                fetchSelectedBudgetUsers(idBudget)
            is SpendingDetailsContract.Action.DeleteSpending -> deleteSpending(idSpending)
            is SpendingDetailsContract.Action.UpdateSpending ->
                updateSpending(idBudget, idSpending, userId, userName, product, date, amount)
        }
    }

    private fun fetchSpending(idBudget: String, idSpending: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    SpendingDetailsContract.Event.RenderSpending(
                        spendingItemToUiTransformer.transform(
                            budgets.find { it.id == idBudget }!!.spending.find { it.id == idSpending }!!
                        )
                    )
                )
            },
                onError = { Log.e(TAG, "Failure to fetch spending", it) }
            ).let(::addDisposable)
    }

    private fun fetchSelectedBudgetUsers(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                val budget = budgets.find { it.id == idBudget }!!
                send(
                    SpendingDetailsContract.Event.RenderSelectedBudgetUsers(
                        mapOf(
                            budget.firstUserName to budget.firstUserId,
                            budget.secondUserName to budget.secondUserId
                        )
                    )
                )
            },
                onError = { Log.e(TAG, "Failure to fetch selected budget users", it) }
            ).let(::addDisposable)
    }

    private fun deleteSpending(idSpending: String) {
        repository.deleteSpending(idSpending)
    }

    private fun updateSpending(
        idBudget: String,
        idSpending: String,
        userId: String,
        userName: String,
        product: String,
        date: String,
        amount: Double
    ) {
        repository.updateSpending(
            Spending(
                id = idSpending,
                idBudget = idBudget,
                idUser = userId,
                nameUser = userName,
                product = product,
                amount = amount,
                date = date
            )
        )
    }
}