package com.katarzynaiwonakrzemien.domain.contract.user

import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter

class UserBudgetPresenter(rxSchedulers: RxSchedulers, private val signedInUser: User) :
    BasePresenter<UserBudgetContract.Event, UserBudgetContract.Action>(rxSchedulers),
    UserBudgetContract.Presenter {

    override fun consume(action: UserBudgetContract.Action) = with(action) {
        when (this) {
            is UserBudgetContract.Action.FetchSignedInUserName -> fetchSignedInUserName()
        }
    }

    private fun fetchSignedInUserName() {
        send(UserBudgetContract.Event.RenderSignedInUserName(signedInUser.name))
    }
}