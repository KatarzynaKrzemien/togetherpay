package com.katarzynaiwonakrzemien.domain.contract.user.budgets

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import io.reactivex.rxkotlin.subscribeBy

class BudgetSelectPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User,
    private val budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
    private val userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
) : BasePresenter<BudgetSelectContract.Event, BudgetSelectContract.Action>(rxSchedulers),
    BudgetSelectContract.Presenter {

    override fun consume(action: BudgetSelectContract.Action) = with(action) {
        when (this) {
            is BudgetSelectContract.Action.FetchBudgets -> fetchBudgets()
            is BudgetSelectContract.Action.FetchUsers -> fetchUsers()
            is BudgetSelectContract.Action.FetchDebtSum -> fetchDebtSum()
            is BudgetSelectContract.Action.FetchOwingSum -> fetchOwingSum()
            is BudgetSelectContract.Action.FetchSignInUserSpendingSum -> fetchSignInUserSpendingSum()
            is BudgetSelectContract.Action.AddBudget -> addBudget(name, idOtherUser)
            is BudgetSelectContract.Action.Refresh -> refresh()
        }
    }

    private fun fetchBudgets() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSelectContract.Event.RenderBudgets(
                        budgets.sortedBy { it.name }.map { budgetItemToUiTransformer.transform(it) })
                )
                send(BudgetSelectContract.Event.RefreshDone)
            },
                onError = { Log.e(TAG, "Failure to fetch budgets", it) }
            ).let(::addDisposable)
    }

    private fun fetchUsers() {
        repository.users().subscribeOn(schedulers.background).observeOn(schedulers.ui)
            .subscribeBy(onNext = { users ->
                send(
                    BudgetSelectContract.Event.RenderUsers(
                        users.filter { it.id != signedInUser.id }
                            .sortedBy { it.name }
                            .map { userItemToUiTransformer.transform(it) })
                )
            },
                onError = { Log.e(TAG, "Failure to fetch users", it) }
            ).let(::addDisposable)
    }

    private fun fetchDebtSum() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSelectContract.Event.RenderDebtSum(budgets.map {
                        budgetItemToUiTransformer.transform(it)
                    }
                        .filter { it.debt.isBelongToSignInUser }
                        .sumByDouble { it.debt.amount })
                )
            },
                onError = { Log.e(TAG, "Failure to fetch debt sum", it) }
            ).let(::addDisposable)
    }

    private fun fetchOwingSum() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSelectContract.Event.RenderOwingSum(
                        budgets.map { budgetItemToUiTransformer.transform(it) }
                            .filterNot { it.debt.isBelongToSignInUser }
                            .sumByDouble { it.debt.amount })
                )
            },
                onError = { Log.e(TAG, "Failure to fetch owing sum", it) }
            ).let(::addDisposable)
    }

    private fun fetchSignInUserSpendingSum() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetSelectContract.Event.RenderSignInUserSpendingSum(
                        budgets.sumByDouble { budget ->
                            budget.spending
                                .filter { it.idUser == signedInUser.id }
                                .sumByDouble { it.amount }
                                .plus(
                                    budget.repaidDebts
                                        .filter { it.idUser == signedInUser.id }
                                        .filter { it.status == Debt.CONFIRMED || it.status == Debt.WAITING }
                                        .sumByDouble { it.amount })
                        }
                    )
                )
            },
                onError = { Log.e(TAG, "Failure to fetch sign in user spending sum", it) }
            ).let(::addDisposable)
    }


    private fun addBudget(name: String, otherUser: UserUi) {
        repository.addBudget(
            Budget(
                name = name,
                firstUserId = signedInUser.id,
                firstUserName = signedInUser.name,
                secondUserId = otherUser.id,
                secondUserName = otherUser.name
            )
        )
    }

    private fun refresh() {
        repository.refreshBudgets()
        repository.refreshUsers()
    }
}