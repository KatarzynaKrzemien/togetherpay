package com.katarzynaiwonakrzemien.domain.contract.user

import com.katarzynaiwonakrzemien.domain.BaseContract

class UserBudgetContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        object FetchSignedInUserName : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderSignedInUserName(val name: String) : Event()
    }
}
