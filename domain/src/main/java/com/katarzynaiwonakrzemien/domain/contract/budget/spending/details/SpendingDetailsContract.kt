package com.katarzynaiwonakrzemien.domain.contract.budget.spending.details

import com.katarzynaiwonakrzemien.domain.BaseContract
import com.katarzynaiwonakrzemien.domain.model.SpendingUi

class SpendingDetailsContract {
    interface Presenter : BaseContract.Presenter<Event, Action>
    interface View : BaseContract.View

    sealed class Action : BaseContract.Action {
        data class FetchSpending(val idBudget: String, val idSpending: String) : Action()
        data class FetchSelectedBudgetUsers(val idBudget: String) : Action()
        data class DeleteSpending(val idSpending: String) : Action()
        data class UpdateSpending(
            val idBudget: String,
            val idSpending: String,
            val userId: String,
            val userName: String,
            val product: String,
            val date: String,
            val amount: Double
        ) : Action()
    }

    sealed class Event : BaseContract.Event {
        data class RenderSpending(val spending: SpendingUi) : Event()
        data class RenderSelectedBudgetUsers(val users: Map<String, String>) : Event()
    }
}
