package com.katarzynaiwonakrzemien.domain.contract.budget

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import io.reactivex.rxkotlin.subscribeBy

class SelectedBudgetPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User
) : BasePresenter<SelectedBudgetContract.Event, SelectedBudgetContract.Action>(rxSchedulers),
    SelectedBudgetContract.Presenter {
    override fun consume(action: SelectedBudgetContract.Action) = with(action) {
        when (this) {
            is SelectedBudgetContract.Action.FetchSignedInUserName -> fetchSignedInUserName()
            is SelectedBudgetContract.Action.FetchBudgetName -> fetchBudgetName(idBudget)
        }
    }

    private fun fetchSignedInUserName() {
        send(SelectedBudgetContract.Event.RenderSignedInUserName(signedInUser.name))
    }

    private fun fetchBudgetName(idBudget: String) {
        repository.budgets()
            .subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(SelectedBudgetContract.Event.RenderBudgetName(budgets.find { it.id == idBudget }!!.name))
            },
                onError = { Log.e(TAG, "Failure to fetch budget name", it) }
            ).let(::addDisposable)
    }
}
