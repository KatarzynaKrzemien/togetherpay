package com.katarzynaiwonakrzemien.domain.contract.budget.details

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import io.reactivex.rxkotlin.subscribeBy

class BudgetDetailsPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User,
    private val budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
    private val budgetUiToUiTransformer: UiToItemTransformer<BudgetUi, Budget>
) : BasePresenter<BudgetDetailsContract.Event, BudgetDetailsContract.Action>(rxSchedulers),
    BudgetDetailsContract.Presenter {

    override fun consume(action: BudgetDetailsContract.Action) = with(action) {
        when (this) {
            is BudgetDetailsContract.Action.FetchBudget -> fetchBudget(idBudget)
            is BudgetDetailsContract.Action.DeleteBudget -> deleteBudget(budget)
            is BudgetDetailsContract.Action.UpdateBudget -> updateBudget(name, budget)
            is BudgetDetailsContract.Action.ClearBudget -> clearBudget(budget, date)
        }
    }

    private fun fetchBudget(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetDetailsContract.Event.RenderBudget(
                        budgetItemToUiTransformer.transform(
                            budgets.find { it.id == idBudget }!!
                        )
                    )
                )
            },
                onError = { Log.e(TAG, "Failure to fetch budget", it) }
            ).let(::addDisposable)
    }

    private fun deleteBudget(budget: BudgetUi) {
        repository.deleteBudget(budgetUiToUiTransformer.transform(budget))
    }

    private fun updateBudget(name: String, budget: BudgetUi) {
        repository.updateBudget(budgetUiToUiTransformer.transform(budget.copy(name = name)))
    }

    private fun clearBudget(budget: BudgetUi, date: String) {
        val waitingDebts = budget.repaidDebts.filter { it.status == DebtUi.Status.WAITING }
        var signedInUserSpending = 0.0
        var otherUserSpending = 0.0

        if (budget.debt.amount > 0.0) {
            if (budget.debt.isBelongToSignInUser) otherUserSpending +=
                budget.debt.amount else signedInUserSpending += budget.debt.amount
        }

        if (waitingDebts.isNotEmpty()) {
            waitingDebts.forEach {
                if (it.amount.isBelongToSignInUser)
                    otherUserSpending += it.amount.amount
                else
                    signedInUserSpending += it.amount.amount
            }
        }

        budget.spendingList.forEach { repository.deleteSpending(it.id) }
        budget.repaidDebts
            .filterNot { it.status == DebtUi.Status.WAITING }
            .forEach { repository.deleteDebt(it.id) }

        if (signedInUserSpending > 0.0) {
            repository.addSpending(
                Spending(
                    idBudget = budget.id,
                    idUser = signedInUser.id,
                    nameUser = signedInUser.name,
                    product = "Staff",
                    amount = signedInUserSpending * 2,
                    date = date
                )
            )
        }

        if (otherUserSpending > 0.0) {
            repository.addSpending(
                Spending(
                    idBudget = budget.id,
                    idUser = budget.otherUser.id,
                    nameUser = budget.otherUser.name,
                    product = "Staff",
                    amount = otherUserSpending * 2,
                    date = date
                )
            )
        }
    }
}
