package com.katarzynaiwonakrzemien.domain.contract.user.debt

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import io.reactivex.rxkotlin.subscribeBy

class UserDebtPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User,
    private val debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>,
    private val budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
    private val debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
) :
    BasePresenter<UserDebtContract.Event, UserDebtContract.Action>(rxSchedulers),
    UserDebtContract.Presenter {
    override fun consume(action: UserDebtContract.Action) = with(action) {
        when (this) {
            is UserDebtContract.Action.FetchDebts -> fetchDebts()
            is UserDebtContract.Action.FetchDebtSum -> fetchDebtSum()
            is UserDebtContract.Action.FetchOwingSum -> fetchOwingSum()
            is UserDebtContract.Action.RejectDebt -> updateDebt(debt, status, updateDate)
            is UserDebtContract.Action.ConfirmDebt -> updateDebt(debt, status, updateDate)
            is UserDebtContract.Action.RepayAgainDebt -> updateDebt(debt, status, updateDate)
            is UserDebtContract.Action.DeleteDebt -> deleteDebt(idDebt)
            is UserDebtContract.Action.AddDebt -> addDebt(idBudget, amount, date)
            is UserDebtContract.Action.Refresh -> refresh()
        }
    }

    private fun fetchDebts() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                val allUserDebtList: MutableList<DebtUi> = mutableListOf()
                budgets.map { budgetItemToUiTransformer.transform(it) }.forEach {
                    if (it.debt.amount != 0.0) {
                        allUserDebtList.add(
                            debtItemToUiTransformer.transform(
                                Debt(
                                    idBudget = it.id,
                                    idUser = if (it.debt.isBelongToSignInUser) signedInUser.id else it.otherUser.id,
                                    nameUser = if (it.debt.isBelongToSignInUser) signedInUser.name else it.otherUser.name,
                                    status = Debt.NOREPAY,
                                    amount = it.debt.amount
                                )
                            )
                        )
                    }
                }

                budgets.map { budgetItemToUiTransformer.transform(it) }.map { it.repaidDebts }
                    .forEach { it.forEach { debt -> allUserDebtList.add(debt) } }
                send(UserDebtContract.Event.RenderDebts(
                    allUserDebtList
                        .asSequence()
                        .sortedByDescending { it.date.day }
                        .sortedByDescending { it.date.month }
                        .sortedByDescending { it.date.year }
                        .sortedByDescending { it.updateDate.day }
                        .sortedByDescending { it.updateDate.month }
                        .sortedByDescending { it.updateDate.year }
                        .toList()
                ))
                send(UserDebtContract.Event.RefreshDone)
            },
                onError = { Log.e(TAG, "Failure to fetch debts", it) }
            ).let(::addDisposable)
    }

    private fun fetchDebtSum() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                val allUserDebtList = listOf<DebtUi>()
                allUserDebtList.plus(budgets.map { it.repaidDebts })
                send(UserDebtContract.Event.RenderDebtSum(
                    budgets.map { budgetItemToUiTransformer.transform(it) }
                        .filter { it.debt.isBelongToSignInUser }
                        .sumByDouble { it.debt.amount })
                )
            },
                onError = { Log.e(TAG, "Failure to fetch debt sum", it) }
            ).let(::addDisposable)
    }

    private fun fetchOwingSum() {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                val allUserDebtList = listOf<DebtUi>()
                allUserDebtList.plus(budgets.map { it.repaidDebts })
                send(UserDebtContract.Event.RenderOwingSum(
                    budgets.map { budgetItemToUiTransformer.transform(it) }
                        .filterNot { it.debt.isBelongToSignInUser }
                        .sumByDouble { it.debt.amount })
                )
            },
                onError = { Log.e(TAG, "Failure to fetch owing sum", it) }
            ).let(::addDisposable)
    }

    private fun updateDebt(debt: DebtUi, status: DebtUi.Status, updateDate: DateUi) {
        repository.updateDebt(
            debtUiToItemTransformer.transform(debt.copy(updateDate = updateDate, status = status))
        )
    }

    private fun deleteDebt(idDebt: String) {
        repository.deleteDebt(idDebt)
    }

    private fun addDebt(idBudget: String, amount: Double, date: String) {
        repository.addDebt(
            Debt(
                idUser = signedInUser.id,
                nameUser = signedInUser.name,
                idBudget = idBudget,
                updateDate = date,
                amount = amount,
                date = date
            )
        )
    }

    private fun refresh() {
        repository.refreshBudgets()
    }
}