package com.katarzynaiwonakrzemien.domain.contract.budget.debt

import com.katarzynaiwonakrzemien.core.extension.TAG
import com.katarzynaiwonakrzemien.core.log.Log
import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.core.repository.RepositoryManager
import com.katarzynaiwonakrzemien.core.rx.RxSchedulers
import com.katarzynaiwonakrzemien.domain.BasePresenter
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer
import io.reactivex.rxkotlin.subscribeBy

class BudgetDebtPresenter(
    rxSchedulers: RxSchedulers,
    private val repository: RepositoryManager,
    private val signedInUser: User,
    private val debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>,
    private val budgetItemToUiTransformer: ItemToUiTransformer<Budget, BudgetUi>,
    private val debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
) : BasePresenter<BudgetDebtContract.Event, BudgetDebtContract.Action>(rxSchedulers),
    BudgetDebtContract.Presenter {

    override fun consume(action: BudgetDebtContract.Action) = with(action) {
        when (this) {
            is BudgetDebtContract.Action.FetchRepaidDebts -> fetchRepaidDebts(idBudget)
            is BudgetDebtContract.Action.FetchCurrentDebt -> fetchCurrentDebt(idBudget)
            is BudgetDebtContract.Action.RejectDebt -> updateDebt(debt, status,updateDate)
            is BudgetDebtContract.Action.ConfirmDebt -> updateDebt(debt, status,updateDate)
            is BudgetDebtContract.Action.RepayAgainDebt -> updateDebt(debt, status,updateDate)
            is BudgetDebtContract.Action.DeleteDebt ->  deleteDebt(idDebt)
            is BudgetDebtContract.Action.AddDebt -> addDebt(idBudget, amount, date)
            is BudgetDebtContract.Action.Refresh -> refresh()
        }
    }

    private fun fetchRepaidDebts(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetDebtContract.Event.RenderRepaidDebts(
                        budgets.find { it.id == idBudget }!!
                            .repaidDebts
                            .asSequence()
                            .map { debtItemToUiTransformer.transform(it) }
                            .sortedByDescending { it.date.day }
                            .sortedByDescending { it.date.month }
                            .sortedByDescending { it.date.year }
                            .sortedByDescending { it.updateDate.day }
                            .sortedByDescending { it.updateDate.month }
                            .sortedByDescending { it.updateDate.year }
                            .toList()
                ))
            },
                onError = { Log.e(TAG, "Failure to fetch repaid debts", it) }
            ).let(::addDisposable)
    }

    private fun fetchCurrentDebt(idBudget: String) {
        repository.budgets().subscribeOn(schedulers.background)
            .observeOn(schedulers.ui)
            .subscribeBy(onNext = { budgets ->
                send(
                    BudgetDebtContract.Event.RenderCurrentDebt(
                        budgetItemToUiTransformer.transform(budgets.find { it.id == idBudget }!!).debt
                    )
                )
                send(BudgetDebtContract.Event.RefreshDone)
            },
                onError = { Log.e(TAG, "Failure to fetch current debt", it) }
            ).let(::addDisposable)
    }

    private fun updateDebt(debt: DebtUi, status: DebtUi.Status, updateDate: DateUi) {
        repository.updateDebt(
            debtUiToItemTransformer.transform(debt.copy(updateDate = updateDate, status = status))
        )
    }

    private fun deleteDebt(idDebt: String) {
        repository.deleteDebt(idDebt)
    }

    private fun addDebt(idBudget: String, amount: Double, date: String) {
        repository.addDebt(
            Debt(
                idUser = signedInUser.id,
                nameUser = signedInUser.name,
                idBudget = idBudget,
                updateDate = date,
                amount = amount,
                date = date
            )
        )
    }

    private fun refresh() {
        repository.refreshBudgets()
    }

}