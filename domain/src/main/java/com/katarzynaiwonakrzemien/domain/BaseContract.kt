package com.katarzynaiwonakrzemien.domain

import io.reactivex.Observable

class BaseContract {
    interface Presenter<E : Event, A : Action> {
        fun attach(view: Observable<A>)
        fun subscribe(): Observable<E>
        fun dispose()
    }

    interface View

    interface Action

    interface Event
}