package com.katarzynaiwonakrzemien.domain.transformer.spending

import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer

class SpendingItemToUiTransformer(
    private val signedInUser: User,
    private val userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
) : ItemToUiTransformer<Spending, SpendingUi> {

    override fun transform(item: Spending): SpendingUi =
        SpendingUi(
            id = item.id,
            idBudget = item.idBudget,
            user = userItemToUiTransformer.transform(User(item.idUser, item.nameUser)),
            product = item.product,
            amount = AmountInfoUi(item.amount, item.idUser == signedInUser.id),
            date = DateUi(item.date)
        )
}