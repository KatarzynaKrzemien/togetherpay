package com.katarzynaiwonakrzemien.domain.transformer.budget

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.BudgetUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer

class BudgetUiToItemTransformer(
    private val signedInUser: User,
    private val spendingUiToItemTransformer: UiToItemTransformer<SpendingUi, Spending>,
    private val debtUiToItemTransformer: UiToItemTransformer<DebtUi, Debt>
) : UiToItemTransformer<BudgetUi, Budget> {

    override fun transform(uiItem: BudgetUi): Budget =
        Budget(
            id = uiItem.id,
            name = uiItem.name,
            firstUserId = signedInUser.id,
            firstUserName = signedInUser.name,
            secondUserId = uiItem.otherUser.id,
            secondUserName = uiItem.otherUser.name,
            spending = uiItem.spendingList.map { spendingUiToItemTransformer.transform(it) },
            repaidDebts = uiItem.repaidDebts.map { debtUiToItemTransformer.transform(it) }
        )
}