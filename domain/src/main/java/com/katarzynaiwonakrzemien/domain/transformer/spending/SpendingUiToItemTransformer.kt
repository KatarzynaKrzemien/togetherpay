package com.katarzynaiwonakrzemien.domain.transformer.spending

import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.domain.model.SpendingUi
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer

class SpendingUiToItemTransformer : UiToItemTransformer<SpendingUi, Spending> {

    override fun transform(uiItem: SpendingUi): Spending =
        Spending(
            id = uiItem.id,
            idBudget = uiItem.idBudget,
            idUser = uiItem.user.id,
            nameUser = uiItem.user.name,
            product = uiItem.product,
            amount = uiItem.amount.amount,
            date = uiItem.date.time
        )
}