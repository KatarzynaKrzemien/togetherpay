package com.katarzynaiwonakrzemien.domain.transformer.budget

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.*
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer
import kotlin.math.abs

class BudgetItemToUiTransformer(
    private val signedInUser: User,
    private val userItemToUiTransformer: ItemToUiTransformer<User, UserUi>,
    private val spendingItemToUiTransformer: ItemToUiTransformer<Spending, SpendingUi>,
    private val debtItemToUiTransformer: ItemToUiTransformer<Debt, DebtUi>
) : ItemToUiTransformer<Budget, BudgetUi> {

    override fun transform(item: Budget): BudgetUi =
        BudgetUi(
            id = item.id,
            name = item.name,
            otherUser = userItemToUiTransformer.transform(getOtherUser(item)),
            spending = item.spending.sumByDouble { it.amount },
            debt = calculateDebt(
                calculateUserSpending(signedInUser.id, item.spending, item.repaidDebts),
                calculateUserSpending(getOtherUser(item).id, item.spending, item.repaidDebts)
            ),
            spendingList = item.spending.map { spendingItemToUiTransformer.transform(it) },
            repaidDebts = item.repaidDebts.map { debtItemToUiTransformer.transform(it) }
        )

    private fun getOtherUser(item: Budget): User =
        if (item.firstUserId == signedInUser.id) User(item.secondUserId, item.secondUserName)
        else User(item.firstUserId, item.firstUserName)

    private fun calculateDebt(signedInUserSpending: Double, otherUserSpending: Double):
            AmountInfoUi = AmountInfoUi(
        abs(signedInUserSpending - otherUserSpending),
        signedInUserSpending < otherUserSpending
    )

    private fun calculateUserSpending(
        idUser: String,
        spending: List<Spending>,
        repaidDebts: List<Debt>
    ): Double =
        spending.filter { it.idUser == idUser }
            .sumByDouble { it.amount }
            .div(2)
            .plus(
                repaidDebts.filter { it.idUser == idUser }
                    .filter { it.status == Debt.CONFIRMED || it.status == Debt.WAITING }
                    .sumByDouble { it.amount }
            )
}