package com.katarzynaiwonakrzemien.domain.transformer

import com.katarzynaiwonakrzemien.core.model.Item
import com.katarzynaiwonakrzemien.domain.model.ItemUi

interface UiToItemTransformer<T1 : ItemUi, T2 : Item> {
    fun transform(uiItem: T1): T2
}