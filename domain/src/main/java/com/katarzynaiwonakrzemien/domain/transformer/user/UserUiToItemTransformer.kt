package com.katarzynaiwonakrzemien.domain.transformer.user

import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer

class UserUiToItemTransformer : UiToItemTransformer<UserUi, User> {

    override fun transform(uiItem: UserUi): User =
        User(id = uiItem.id, name = uiItem.name)
}