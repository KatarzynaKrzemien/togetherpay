package com.katarzynaiwonakrzemien.domain.transformer.debt

import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.AmountInfoUi
import com.katarzynaiwonakrzemien.domain.model.DateUi
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer

class DebtItemToUiTransformer(
    private val signedInUser: User,
    private val userItemToUiTransformer: ItemToUiTransformer<User, UserUi>
) : ItemToUiTransformer<Debt, DebtUi> {

    override fun transform(item: Debt): DebtUi = DebtUi(
        id = item.id,
        user = userItemToUiTransformer.transform(User(item.idUser, item.nameUser)),
        idBudget = item.idBudget,
        status = when (item.status) {
            Debt.CONFIRMED -> DebtUi.Status.CONFIRMED
            Debt.NOREPAY -> DebtUi.Status.NOREPAY
            Debt.REJECTED -> DebtUi.Status.REJECTED
            Debt.WAITING -> DebtUi.Status.WAITING
            else -> throw(IllegalArgumentException())
        },
        updateDate = DateUi(item.updateDate),
        amount = AmountInfoUi(item.amount, item.idUser == signedInUser.id),
        date = DateUi(item.date)
    )
}