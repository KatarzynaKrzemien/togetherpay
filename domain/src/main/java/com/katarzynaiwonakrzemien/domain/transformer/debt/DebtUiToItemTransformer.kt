package com.katarzynaiwonakrzemien.domain.transformer.debt

import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.domain.model.DebtUi
import com.katarzynaiwonakrzemien.domain.transformer.UiToItemTransformer

class DebtUiToItemTransformer : UiToItemTransformer<DebtUi, Debt> {

    override fun transform(uiItem: DebtUi): Debt =
        Debt(
            id = uiItem.id,
            idBudget = uiItem.idBudget,
            idUser = uiItem.user.id,
            nameUser = uiItem.user.name,
            status = when (uiItem.status) {
                DebtUi.Status.REJECTED -> Debt.REJECTED
                DebtUi.Status.CONFIRMED -> Debt.CONFIRMED
                DebtUi.Status.WAITING -> Debt.WAITING
                DebtUi.Status.NOREPAY -> Debt.NOREPAY
            },
            updateDate = uiItem.updateDate.time,
            amount = uiItem.amount.amount,
            date = uiItem.date.time
        )
}