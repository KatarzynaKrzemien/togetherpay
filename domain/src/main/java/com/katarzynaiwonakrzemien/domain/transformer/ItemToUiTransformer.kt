package com.katarzynaiwonakrzemien.domain.transformer

import com.katarzynaiwonakrzemien.core.model.Item
import com.katarzynaiwonakrzemien.domain.model.ItemUi

interface ItemToUiTransformer<T1 : Item, T2 : ItemUi> {
    fun transform(item: T1): T2
}