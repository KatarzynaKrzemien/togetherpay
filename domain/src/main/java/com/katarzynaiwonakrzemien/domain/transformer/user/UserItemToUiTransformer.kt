package com.katarzynaiwonakrzemien.domain.transformer.user

import com.katarzynaiwonakrzemien.core.model.User
import com.katarzynaiwonakrzemien.domain.model.UserUi
import com.katarzynaiwonakrzemien.domain.transformer.ItemToUiTransformer

class UserItemToUiTransformer : ItemToUiTransformer<User, UserUi> {

    override fun transform(item: User): UserUi = UserUi(id = item.id, name = item.name)
}