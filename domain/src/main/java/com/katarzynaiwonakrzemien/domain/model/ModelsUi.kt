package com.katarzynaiwonakrzemien.domain.model

import java.text.SimpleDateFormat
import java.util.*

data class UserUi(
    override val id: String,
    val name: String
) : ItemUi

data class BudgetUi(
    override val id: String,
    val name: String,
    val otherUser: UserUi,
    val spending: Double,
    val debt: AmountInfoUi,
    val spendingList: List<SpendingUi>,
    val repaidDebts: List<DebtUi>
) : ItemUi

data class SpendingUi(
    override val id: String,
    val idBudget: String,
    val user: UserUi,
    val product: String,
    val amount: AmountInfoUi,
    val date: DateUi
) : ItemUi

data class DebtUi(
    override val id: String,
    val user: UserUi,
    val idBudget: String,
    val status: Status,
    val updateDate: DateUi,
    val amount: AmountInfoUi,
    val date: DateUi
) : ItemUi {
    enum class Status {
        NOREPAY, CONFIRMED, WAITING, REJECTED
    }
}

data class AmountInfoUi(
    val amount: Double,
    val isBelongToSignInUser: Boolean
)

data class DateUi(val time: String) {

    val year: Int = if (time.isNotEmpty()) time.split(split)[2].toInt() else 0
    val month: Int = if (time.isNotEmpty()) time.split(split)[1].toInt().minus(1) else 0
    val day: Int = if (time.isNotEmpty()) time.split(split)[0].toInt() else 0

    companion object {
        private const val split = ".";
        fun createDate(day: Int, month: Int, year: Int): String =
            SimpleDateFormat("dd${split}MM${split}YYYY").format(
                Calendar.getInstance().apply {
                    set(Calendar.DAY_OF_MONTH, day)
                    set(Calendar.MONTH, month)
                    set(Calendar.YEAR, year)
                }.time
            )
    }
}