package com.katarzynaiwonakrzemien.core.log

interface Log {
    fun d(tag: String = "Log", message: String)
    fun w(tag: String = "Log", message: String, throwable: Throwable? = null)
    fun e(tag: String = "Log", message: String, throwable: Throwable? = null)
    fun i(tag: String = "Log", message: String)
    fun wtf(tag: String = "Log", message: String)

    companion object : Log {
        private val loggers = mutableListOf<Log>()
        fun add(log: Log) = loggers.add(log)
        override fun d(tag: String, message: String) = loggers.forEach { it.d(tag, message) }
        override fun e(tag: String, message: String, throwable: Throwable?) =
            loggers.forEach { it.e(tag, message, throwable) }
        override fun w(tag: String, message: String, throwable: Throwable?) =
            loggers.forEach { it.w(tag, message, throwable) }
        override fun i(tag: String, message: String) = loggers.forEach { it.i(tag, message) }
        override fun wtf(tag: String, message: String) = loggers.forEach { it.wtf(tag, message) }
    }
}