package com.katarzynaiwonakrzemien.core.extension

import java.math.BigDecimal
import java.math.RoundingMode

val Any.TAG
    get() = this.javaClass.simpleName

fun Double.toRoundedString() =
    BigDecimal(this).setScale(2, RoundingMode.HALF_UP).toString()