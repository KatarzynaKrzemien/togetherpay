package com.katarzynaiwonakrzemien.core.model

data class User(
    override var id: String = defaultId(),
    var name: String = ""
) : Item() {
    override fun toHashMap(): HashMap<String, Any> = hashMapOf("id" to id, "name" to name)
}

data class Budget(
    override var id: String = defaultId(),
    var name: String = "",
    var firstUserId: String = "",
    var firstUserName: String = "",
    var secondUserId: String = "",
    var secondUserName: String = "",
    var spending: List<Spending> = listOf(),
    var repaidDebts: List<Debt> = listOf()
) : Item() {

    override fun toHashMap() = hashMapOf(
        "id" to id,
        "name" to name,
        "firstUserId" to firstUserId,
        "firstUserName" to firstUserName,
        "secondUserId" to secondUserId,
        "secondUserName" to secondUserName
    )
}

data class Spending(
    override var id: String = defaultId(),
    var idBudget: String = "",
    var idUser: String = "",
    var nameUser: String = "",
    var product: String = "",
    var amount: Double = 0.0,
    var date: String = ""
) : Item() {

    override fun toHashMap() = hashMapOf(
        "id" to id,
        "idBudget" to idBudget,
        "idUser" to idUser,
        "nameUser" to nameUser,
        "product" to product,
        "amount" to amount,
        "date" to date
    )
}

data class Debt(
    override var id: String = defaultId(),
    var idBudget: String = "",
    var idUser: String = "",
    var nameUser: String = "",
    var status: Int = WAITING,
    var updateDate: String = "",
    var amount: Double = 0.0,
    var date: String = ""
) : Item() {

    override fun toHashMap() = hashMapOf(
        "id" to id,
        "idBudget" to idBudget,
        "idUser" to idUser,
        "nameUser" to nameUser,
        "status" to status,
        "updateDate" to updateDate,
        "amount" to amount,
        "date" to date
    )

    companion object {
        const val NOREPAY: Int = 0
        const val CONFIRMED: Int = 1
        const val WAITING: Int = 2
        const val REJECTED: Int = 3
    }
}