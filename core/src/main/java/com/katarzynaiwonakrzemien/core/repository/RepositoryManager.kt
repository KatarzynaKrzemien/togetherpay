package com.katarzynaiwonakrzemien.core.repository

interface RepositoryManager : Repository {
    fun cleanData()
}