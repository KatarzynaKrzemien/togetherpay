package com.katarzynaiwonakrzemien.core.repository

import com.katarzynaiwonakrzemien.core.model.Budget
import com.katarzynaiwonakrzemien.core.model.Debt
import com.katarzynaiwonakrzemien.core.model.Spending
import com.katarzynaiwonakrzemien.core.model.User
import io.reactivex.Observable

interface Repository {
    fun budgets(): Observable<List<Budget>>
    fun users(): Observable<List<User>>
    fun addUserIfNotExist(user: User)
    fun addBudget(budget: Budget)
    fun addSpending(spending: Spending)
    fun addDebt(debt: Debt)
    fun deleteBudget(budget: Budget)
    fun deleteSpending(id: String)
    fun deleteDebt(id: String)
    fun updateDebt(debt: Debt)
    fun updateBudget(budget: Budget)
    fun updateSpending(spending: Spending)
    fun refreshBudgets()
    fun refreshUsers()
}