package com.katarzynaiwonakrzemien.core.rx

import io.reactivex.Scheduler

interface RxSchedulers {
    val background: Scheduler
    val ui: Scheduler
}